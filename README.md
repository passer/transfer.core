**文件批量传输组件**

文件批量传输组件适用于后台服务器之间文件批量传输，能对某个文件夹进行自动监控、多线程批量传输、文件md5对比、错误重传，也能满足中转传输情况(外网服务器把文件传至中转服务器，中转服务器触发再将文件传送至目标服务器，传输结果消息返回给源服务器)。

1. 可直接独立成单独的客户端和服务端系统，支持配置。
2. 可作为工具架包，其他系统可调用方法进行文件传输相关操作
3. 支持文件夹监控，自动传输
4. 支持各种类型的文件多线程传输
5. 支持文件md5对比，错误重传
6. 支持中转传输
7. 支持安全权限验证
8. 可自定义拦截器、回调类，易扩展

#### 直接使用(编译好的文件在build文件夹下):

####1、两台主机之间传输配置：
	两台主机之间传输时，发送文件主机要部署transfer_client项目；接收文件主机部署transfer_server项目。
#####a.发送文件的主机部署时，修改里面的sysinfo.properties信息，然后打开transfer_client.bat进行文件夹监控:
```
#要监控的文件夹，当isMonitor=true有效
monitorPath=E:\\upload
#是否用md5校验
isMd5Check=true
#是否中转传输
isTransit=false
#文件要发送给的主机ip，如果isTransit为true是中转服务器ip，false则为目标ip
sendToIp=localhost
#文件要发送给的主机端口，如果isTransit为true是中转服务器端口，false则为目标端口
sendToPort=111
#目标主机的ip，如果isTransit为true则无效
targetIp=
#目标主机的端口，如果isTransit为true则无效
targetPort=
#接收消息端口
receiveMsgPort=
#定时任务每隔多少时间执行一次，单位毫秒
timerTaskPeriod=50000
#超时时间,毫秒
sendTimeout=18000000
#错误重传次数
failResendCount=3
#是否进行安全权限验证（如果文件接收服务器开启了，必须开启），为true会开启权限验证，其内部为增加一个验证拦截器
isAuthorityCheck=false
#权限验证密锁（要跟文件接收服务器对应），必须isAuthorityCheck为true时有效。
authorityKey=password111
#缓存名，如果多个文件夹监听任务要用不同的缓存名，这里由于只有一个监听任务，可不写
cacheName=cachename1
```
#####b.文件接收的主机是修改transfer_server项目下fileserver.xml，运行transfer_server.bat启动:
```
<?xml version="1.0" encoding="utf-8"?>
<config>
	<!-- 
		port:服务器接收端口  
		filePath:文件接收保存的路径  
		soTimeout:文件接收超时时间    
		 poolSize：文件接收线程池大小  
		 isTransitServer：是否作为中转服务器
		 isAuthorityCheck：是否需要安全权限验证（需要拦截器中加入AuthorityServerFilePlugin插件），如果为true，客户端一定也要开启权限验证，并且authorityKey要相等
		 authorityKey：密锁，当isAuthorityCheck为true有效
		 -->
	<fileserver port="111" filePath="E:\test\file" soTimeout="" poolSize="5" isTransitServer="false" isAuthorityCheck="false" authorityKey="password111" >
	 	 	<!-- 拦截器 -->
	 	<plugins>
	 		<plugin class="com.filetransfer.plugin.LogServerFilePlugin"  ></plugin>
	 		
	 		<plugin class="com.filetransfer.plugin.AuthorityServerFilePlugin" >
	 		<param>serverReceiveControl</param>
	 		</plugin>
	 	</plugins>
	 	
	</fileserver>

</config>
```


####2、中转传输配置：
	一台主机把文件传输给中转服务器，中转服务器再把文件传输至第三台主机时，接收文件主机和中转主机都要部署transfer_server项目。文件发送主机使用transfer_client项目。
 
#####a. 发送文件的主机部署transfer_client项目，修改sysinfo.properties信息，然后打开transfer_client.bat进行文件夹监控:
```
#要监控的文件夹，当isMonitor=true有效
monitorPath=E:\\upload
#是否用md5校验
isMd5Check=true
#是否中转传输
isTransit=true
#文件要发送给的主机ip，如果isTransit为true是中转服务器ip，false则为目标ip
sendToIp=192.168.1.100
#文件要发送给的主机端口，如果isTransit为true是中转服务器端口，false则为目标端口
sendToPort=111
#目标主机的ip，如果isTransit为true则无效
targetIp= 192.168.1.101
#目标主机的端口，如果isTransit为true则无效
targetPort=102
#接收消息端口
receiveMsgPort=201
#定时任务每隔多少时间执行一次，单位毫秒
timerTaskPeriod=50000
#超时时间,毫秒
sendTimeout=18000000
#错误重传次数
failResendCount=3
#是否进行安全权限验证（如果文件接收服务器开启了，必须开启），为true会开启权限验证，其内部为增加一个验证拦截器
isAuthorityCheck=false
#权限验证密锁（要跟文件接收服务器对应），必须isAuthorityCheck为true时有效。
authorityKey=password111
#缓存名，如果多个文件夹监听任务要用不同的缓存名，这里由于只有一个监听任务，可不写
cacheName=cachename1
```

#####b.中转服务器部署transfer_server项目，修改配置信息fileserver.xml文件，打开transfer_server.bat进行启动:

```
	<?xml version="1.0" encoding="utf-8"?>
<config>
	<!-- 
		port:服务器接收端口  
		filePath:文件接收保存的路径  
		soTimeout:文件接收超时时间    
		 poolSize：文件接收线程池大小  
		 isTransitServer：是否作为中转服务器
		 isAuthorityCheck：是否需要安全权限验证（需要拦截器中加入AuthorityServerFilePlugin插件），如果为true，客户端一定也要开启权限验证，并且authorityKey要相等
		 authorityKey：密锁，当isAuthorityCheck为true有效
		 -->
	<fileserver port="111" filePath="E:\test\file" soTimeout="" poolSize="5" isTransitServer="true" isAuthorityCheck="false" authorityKey="password111" >
	
		<!-- 文件接收回调，
		class为类的路径
		-->
	<!-- 	<fileReceiveCallBack class="com.filetransfer.server.fileserver.callback.TransitFileReceiveCallBack"  >
			<param>serverReceiveControl</param>
		</fileReceiveCallBack> -->
		
		<!-- 中转时发给源主机消息的适配器 -->
	<!-- 	<sendMessageAdapter class="com.filetransfer.client.message.defaultadapter.EdenSendMessageAdapter"  >
		</sendMessageAdapter> -->
		
	 	<!-- 拦截器 -->
	 	 	<!-- 拦截器 -->
	 	<plugins>
	 		<plugin class="com.filetransfer.plugin.LogServerFilePlugin"  ></plugin>
	 		
	 		<plugin class="com.filetransfer.plugin.AuthorityServerFilePlugin" >
	 		<param>serverReceiveControl</param>
	 		</plugin>
	 	</plugins>
	 	
	</fileserver>

</config>
```
#####c. 目标文件接收的主机也是部署transfer_server项目，修改配置信息fileserver.xml文件，打开transfer_server.bat进行启动:

```
<?xml version="1.0" encoding="utf-8"?>
<config>
	<!-- 
		port:服务器接收端口  
		filePath:文件接收保存的路径  
		soTimeout:文件接收超时时间    
		 poolSize：文件接收线程池大小  
		 isTransitServer：是否作为中转服务器
		 isAuthorityCheck：是否需要安全权限验证（需要拦截器中加入AuthorityServerFilePlugin插件），如果为true，客户端一定也要开启权限验证，并且authorityKey要相等
		 authorityKey：密锁，当isAuthorityCheck为true有效
		 -->
	<fileserver port="112" filePath="E:\cfile" soTimeout="" poolSize="5" isTransitServer="false" isAuthorityCheck="false" authorityKey="password111" >
	 	<!-- 拦截器 -->
	 		<plugins>
	 		<plugin class="com.filetransfer.plugin.LogServerFilePlugin"  ></plugin>
	 		
	 		<plugin class="com.filetransfer.plugin.AuthorityServerFilePlugin" >
	 		<param>serverReceiveControl</param>
	 		</plugin>
	 	</plugins>
	</fileserver>
</config>
```
#### 其他
作为架包调用见： http://my.oschina.net/passerman/blog/715744  
原理说明见： http://my.oschina.net/passerman/blog/715707  



