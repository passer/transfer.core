
package com.filetransfer.transport.command;

import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.filetransfer.transport.BaseMessage;


/**
 * 功能说明: <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-16 <br>
 * 审核人员: <br>
 * 相关文档: <br>
 * 修改记录: <br>
 * 修改日期 修改人员 修改说明 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class PropertieMessage implements BaseMessage,Cloneable {

	private Map<String, Object> properties;
	
	/**
	 * 复制一份副本
	 * @return
	 */
	public PropertieMessage copyto()
	{
		PropertieMessage newMessage = null;
        try{
        	newMessage = (PropertieMessage)clone();
        }catch(CloneNotSupportedException e){
            e.printStackTrace();
        }
        return newMessage;
	}
	

	public void setObjectProperty(String name, Object value) throws Exception {

		if (name == null || name.equals("")) {
			throw new IllegalArgumentException(
					"Property name cannot be empty or null");
		}
		checkValidObject(value);

		
		getProperties().put(name, value);

	}

	public void setBooleanProperty(String name, boolean value) throws Exception {
		setObjectProperty(name, Boolean.valueOf(value));
	}

	public void setByteProperty(String name, byte value) throws Exception {
		setObjectProperty(name, Byte.valueOf(value));
	}

	public void setShortProperty(String name, short value) throws Exception {
		setObjectProperty(name, Short.valueOf(value));
	}

	public void setIntProperty(String name, int value) throws Exception {
		setObjectProperty(name, Integer.valueOf(value));
	}

	public void setLongProperty(String name, long value) throws Exception {
		setObjectProperty(name, Long.valueOf(value));
	}

	public void setFloatProperty(String name, float value) throws Exception {
		setObjectProperty(name, new Float(value));
	}

	public void setDoubleProperty(String name, double value) throws Exception {
		setObjectProperty(name, new Double(value));
	}

	public void setStringProperty(String name, String value) throws Exception {
		setObjectProperty(name, value);
	}

	protected void checkValidObject(Object value) throws Exception {

		boolean valid = value instanceof Boolean || value instanceof Byte
				|| value instanceof Short || value instanceof Integer
				|| value instanceof Long;
		valid = valid || value instanceof Float || value instanceof Double
				|| value instanceof Character || value instanceof String
				|| value == null;

		if (!valid) {
			throw new RuntimeException(
					"value的值的类型不正确  值: "
							+ value + " type: " + value.getClass());
		}

	}

	public boolean getBooleanProperty(String name) throws Exception {
		Object ob=getObjectProperty(name);
		if(ob!=null)
		{
			return (Boolean) ob;
		}
		return false;
	}

	public byte getByteProperty(String name) throws Exception {
		Object ob=getObjectProperty(name);
		if(ob!=null)
		{
			return (Byte) ob;
		}
		return 0;
	}

	public short getShortProperty(String name) throws Exception {
		Object ob=getObjectProperty(name);
		if(ob!=null)
		{
			return (Short) ob;
		}
		return 0;
	}

	public int getIntProperty(String name) throws Exception {
		Object ob=getObjectProperty(name);
		if(ob!=null)
		{
			return (Integer) ob;
		}
		return 0;
	}

	public long getLongProperty(String name) throws Exception {
		Object ob=getObjectProperty(name);
		if(ob!=null)
		{
			return (Long) ob;
		}
		return 0;
	}

	public float getFloatProperty(String name) throws Exception {
		Object ob=getObjectProperty(name);
		if(ob!=null)
		{
			return (Float) ob;
		}
		return 0;
	}

	public double getDoubleProperty(String name) throws Exception {
		Object ob=getObjectProperty(name);
		if(ob!=null)
		{
			return (Double) ob;
		}
		return 0;
	}

	public String getStringProperty(String name) throws Exception {
		Object ob=getObjectProperty(name);
		if(ob!=null)
		{
			return (String) ob;
		}
		return null;
	}

	public Object getObjectProperty(String name) throws Exception {
		
		return getProperties().get(name);
	}

	public Enumeration getPropertyNames() throws Exception {
		 Vector<String> result = new Vector<String>(this.getProperties().keySet());

        return result.elements();
	}
	
	public Map<String, Object> getProperties() throws IOException {
		if (properties == null) {
			properties = new HashMap<String, Object>();

		}
	    return properties;
	}

}
