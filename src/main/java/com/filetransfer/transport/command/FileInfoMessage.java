
package com.filetransfer.transport.command;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-16 <br>
 * ======== ====== ============================================ <br>
 * 
 */
@XStreamAlias("FileInfoMessage")
public class FileInfoMessage extends PropertieMessage {

	
	private String fileId;
	
	private String absolutePath;//绝对路径
	
	private Long fileSize;
	
	private String md5;
	
	//private String toHostString;  //怎样传送的路径字符串
	
	private Integer state;  //状态  0 开始传输  1 B服务器 传输成功  2 B服务器传输失败   3 C服务器成功  4 C服务器失败
	private String stateInfo;  //状态中文信息
	
	private Long startSendLongTime;  //开始发送文件时间
	
	private String wantRelativePath; //想要保存的相对路径
	
	private String sendToIp;
	
	private Integer sendToPort;
	
	private Boolean isMd5Check;  //是否md5校验
	
	private Boolean isTransit;  //是否中转
	
	
	/**
	 * 目标主机的ip，如果isTransit为true则无效
	 */
	private String targetIp; 
	/**
	 * 目标主机的端口，如果isTransit为true则无效
	 */
	private Integer targetPort; 
	
	private String replyIp; // 回应时的ip
	
	private Integer replyPort; // 回应时的端口，即发送者监听的端口

	private String replyInfo; //发送返回消息
	
	/**
	 * 来路ip
	 */
	private String fromIp;
	
	
	
	
	
	@Override
	public FileInfoMessage copyto() {
		
		return (FileInfoMessage)super.copyto();
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}





	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}



	public String getSendToIp() {
		return sendToIp;
	}

	public void setSendToIp(String sendToIp) {
		this.sendToIp = sendToIp;
	}



	public String getTargetIp() {
		return targetIp;
	}

	public void setTargetIp(String targetIp) {
		this.targetIp = targetIp;
	}

	public Integer getTargetPort() {
		return targetPort;
	}

	public void setTargetPort(Integer targetPort) {
		this.targetPort = targetPort;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}



	public Boolean isMd5Check() {
		return isMd5Check;
	}

	public void setMd5Check(Boolean isMd5Check) {
		this.isMd5Check = isMd5Check;
	}

/*	public String getToHostString() {
		return toHostString;
	}

	public void setToHostString(String toHostString) {
		this.toHostString = toHostString;
	}*/

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public Boolean isTransit() {
		return isTransit;
	}

	public void setTransit(Boolean isTransit) {
		this.isTransit = isTransit;
	}

	public String getWantRelativePath() {
		return wantRelativePath;
	}

	public void setWantRelativePath(String wantRelativePath) {
		this.wantRelativePath = wantRelativePath;
	}

	public Integer getSendToPort() {
		return sendToPort;
	}

	public void setSendToPort(Integer sendToPort) {
		this.sendToPort = sendToPort;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getStateInfo() {
		return stateInfo;
	}

	public void setStateInfo(String stateInfo) {
		this.stateInfo = stateInfo;
	}

	public String getReplyIp() {
		return replyIp;
	}

	public void setReplyIp(String replyIp) {
		this.replyIp = replyIp;
	}

	public Integer getReplyPort() {
		return replyPort;
	}

	public void setReplyPort(Integer replyPort) {
		this.replyPort = replyPort;
	}

	public String getFromIp() {
		return fromIp;
	}

	public void setFromIp(String fromIp) {
		this.fromIp = fromIp;
	}

	public static void main(String[] args) {
		
		
		FileInfoMessage f1=new FileInfoMessage();
		
		f1.setFileId("1111");
		FileInfoMessage f2=f1.copyto();
		f2.setFileId("2222");
		System.out.println(f1.getFileId());
		System.out.println(f2.getFileId());
		
	}

	public String getReplyInfo() {
		return replyInfo;
	}

	public void setReplyInfo(String replyInfo) {
		this.replyInfo = replyInfo;
	}

	public Long getStartSendLongTime() {
		return startSendLongTime;
	}

	public void setStartSendLongTime(Long startSendLongTime) {
		this.startSendLongTime = startSendLongTime;
	}

	
	
	
}
