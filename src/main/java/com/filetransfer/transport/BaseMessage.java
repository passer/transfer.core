
package com.filetransfer.transport;

import java.util.Enumeration;


/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-16 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public interface BaseMessage {

	
    boolean getBooleanProperty(String name) throws Exception;

    byte getByteProperty(String name) throws Exception;

    short getShortProperty(String name) throws Exception;

    int getIntProperty(String name) throws Exception;

    long getLongProperty(String name) throws Exception;

    float getFloatProperty(String name) throws Exception;

    double getDoubleProperty(String name) throws Exception;

    String getStringProperty(String name) throws Exception;

    Object getObjectProperty(String name) throws Exception;

    Enumeration getPropertyNames() throws Exception;

    void setBooleanProperty(String name, boolean value) throws Exception;

    void setByteProperty(String name, byte value) throws Exception;

    void setShortProperty(String name, short value) throws Exception;

    void setIntProperty(String name, int value) throws Exception;

    void setLongProperty(String name, long value) throws Exception;

    void setFloatProperty(String name, float value) throws Exception;

    void setDoubleProperty(String name, double value) throws Exception;

    void setStringProperty(String name, String value) throws Exception;

    void setObjectProperty(String name, Object value) throws Exception;
	
}
