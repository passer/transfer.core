
package com.filetransfer.transport.util;

import com.filetransfer.config.FileClientConfig;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.eis.HostInfo;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.util.LogUtil;
import com.filetransfer.util.StringUtil;
import com.filetransfer.util.ToHostParseUtil;



/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-17 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileInfoMessageUtil {

	public static FileInfoMessage fileInfoToFileInfoMessage(FileInfo fileInfo,boolean isTransit)
	{
		if(fileInfo==null)
		{
			throw new RuntimeException("fileInfo 不能为空！");
		}
		FileInfoMessage fileInfoMessage=new FileInfoMessage();
		
		fileInfoMessage.setFileId(fileInfo.getId());
		fileInfoMessage.setAbsolutePath(fileInfo.getFilePath());
		fileInfoMessage.setFileSize(Long.parseLong(fileInfo.getFileSize()));
		fileInfoMessage.setMd5(fileInfo.getMd5());
		
		if(fileInfo.getToHostString()!=null)
		{
			if(isTransit) //是中转
			{
				HostInfo toHostInfo=ToHostParseUtil.getThroughHostInfo(fileInfo.getToHostString());
				fileInfoMessage.setSendToIp(toHostInfo.getIp());
				fileInfoMessage.setSendToPort(toHostInfo.getPort());
				toHostInfo=ToHostParseUtil.getTargetHostInfo(fileInfo.getToHostString());
				fileInfoMessage.setTargetIp(toHostInfo.getIp());
				fileInfoMessage.setTargetPort(toHostInfo.getPort());
				toHostInfo=ToHostParseUtil.getReceiveMsgHostInfo(fileInfo.getToHostString());
				fileInfoMessage.setReplyPort(toHostInfo.getPort());
				
			}else{
				
					HostInfo toHostInfo=ToHostParseUtil.getTargetHostInfo(fileInfo.getToHostString());
					fileInfoMessage.setSendToIp(toHostInfo.getIp());
					fileInfoMessage.setSendToPort(toHostInfo.getPort());
			}
				
		}
		
		
		//fileInfoMessage.setToHostString(fileInfo.getToHostString());
		
		fileInfoMessage.setState(fileInfo.getState());
		fileInfoMessage.setStateInfo(fileInfo.getStateInfo());
		return fileInfoMessage;
	}
	
	public static FileInfo fileInfoMessageToFileInfo(FileInfoMessage fileInfoMessage)
	{
		if(fileInfoMessage==null)
		{
			throw new RuntimeException("fileInfoMessage 不能为空！");
		}
		FileInfo fileInfo=new FileInfo();
		fileInfo.setId(fileInfoMessage.getFileId());
		fileInfo.setFilePath(fileInfoMessage.getAbsolutePath());
		fileInfo.setFileSize(fileInfoMessage.getFileSize()+"");
		fileInfo.setMd5(fileInfoMessage.getMd5());
		//fileInfo.setToHostString(fileInfoMessage.getToHostString());
		fileInfo.setState(fileInfoMessage.getState());
		fileInfo.setStateInfo(fileInfoMessage.getStateInfo());
		return fileInfo;
	}
	
	/**
	 * 验证相关只是否存在
	 * @param fileInfoMessage
	 */
	public static void checkFileInfoMessage(FileInfoMessage fileInfoMessage)
	{
		if(fileInfoMessage!=null)
		{
			if(StringUtil.isAllNotEmpty(fileInfoMessage.isMd5Check(),fileInfoMessage.isTransit(),fileInfoMessage.getFileId(),fileInfoMessage.getWantRelativePath(),fileInfoMessage.getAbsolutePath(),fileInfoMessage.getSendToIp())&&fileInfoMessage.getSendToPort()!=null)
			{
				//都为不空的
				if(fileInfoMessage.isMd5Check())
				{
					//md5不能为空
					if(!StringUtil.isNotEmpty(fileInfoMessage.getMd5()))
					{
						throw new RuntimeException("fileInfoMessageshi md5验证，但没md5值！");
					}
				}
				
				if(fileInfoMessage.isTransit())
				{
					if(!StringUtil.isNotEmpty(fileInfoMessage.getTargetIp())||fileInfoMessage.getTargetPort()==null||fileInfoMessage.getReplyPort()==null)
					{
						throw new RuntimeException("fileInfoMessage 中转传输 TargetIp,TargetPort,ReplyPort 不能为空！");
						
					}
				}
				
				
				return ;
			}
			
		}
			
		throw new RuntimeException("fileInfoMessage 缺少相关属性或属性不正确！");
		
		
	}
	
	
	public static void putConfigToFileInfoMessage(FileClientConfig clientConfig,FileInfoMessage fileInfoMessage)
	{
		if(clientConfig==null||fileInfoMessage==null)
		{
			throw new RuntimeException("FileInfoMessageUtil.class的putConfigToFileInfoMessage方式 clientConfig,fileInfoMessage不能为null");
		}
		if(fileInfoMessage.getSendToIp()==null)
		{
			fileInfoMessage.setSendToIp(clientConfig.getSendToIp());
		}
		if(fileInfoMessage.getSendToPort()==null)
		{
			fileInfoMessage.setSendToPort(clientConfig.getSendToPort());
		}
		if(fileInfoMessage.isMd5Check()==null)
		{
			fileInfoMessage.setMd5Check(clientConfig.isIsMd5Check());
		}
		if(fileInfoMessage.isTransit()!=null)
		{
			fileInfoMessage.setTransit(clientConfig.isTransit());
		}
		
		if(clientConfig.isTransit()) //是中转
		{
			fileInfoMessage.setTargetIp(clientConfig.getTargetIp());
			fileInfoMessage.setTargetPort(clientConfig.getTargetPort());
			fileInfoMessage.setReplyPort(clientConfig.getReceiveMsgPort());
			
		}
		
	}
	

	/**
	 *  当前是中转服务器，将target相关信息转给sendto
	 * @param fileInfoMessage
	 * @param isTransit
	 */
	public static void changeTarget2SendTo(FileInfoMessage fileInfoMessage,boolean isTransit)
	{
		if(isTransit)
		{
			if(fileInfoMessage.getTargetIp()!=null&&fileInfoMessage.getTargetPort()!=null)
			{
				fileInfoMessage.setSendToIp(fileInfoMessage.getTargetIp());
				fileInfoMessage.setSendToPort(fileInfoMessage.getTargetPort());
				fileInfoMessage.setTargetIp(null);
				fileInfoMessage.setTargetPort(null);
			}else{
				LogUtil.err("fileInfoMessage 信息中转变化时 targetIp，targetPort不能为空",FileInfoMessageUtil.class);
				throw new RuntimeException("fileInfoMessage 信息中转变化时 targetIp，targetPort不能为空");
			}
			
			
		}else{
			
			LogUtil.echo("fileid "+fileInfoMessage.getFileId()+" 当前不是中转，不用变化！",FileInfoMessageUtil.class);
			throw new RuntimeException("fileid "+fileInfoMessage.getFileId()+" 当前不是中转，不用变化！");
		}
		
	}
	
	
	/**
	 * 当前是中转服务器，将Reply变化赋给sendto
	 * @param fileInfoMessage
	 * @param isTransit
	 */
	public static void changeReply2SendTo(FileInfoMessage fileInfoMessage,boolean isTransit)
	{
		if(isTransit)
		{
			if(fileInfoMessage.getReplyIp()!=null&& fileInfoMessage.getReplyPort()!=null)
			{
				fileInfoMessage.setSendToIp(fileInfoMessage.getReplyIp());
				fileInfoMessage.setSendToPort(fileInfoMessage.getReplyPort());
				fileInfoMessage.setReplyIp(null);
				fileInfoMessage.setReplyPort(null);
				
			}else{
				LogUtil.err("fileInfoMessage 信息中转变化时 replyIp，replyPort不能为空",FileInfoMessageUtil.class);
				throw new RuntimeException("fileInfoMessage 信息中转变化时 replyIp，replyPort不能为空");
			}
			
			
		}else{
			
			LogUtil.err("fileid "+fileInfoMessage.getFileId()+" 当前不是中转，不用变化！",FileInfoMessageUtil.class);
			throw new RuntimeException("fileid "+fileInfoMessage.getFileId()+" 当前不是中转，不用变化！");
		}
		
	}
	
}
