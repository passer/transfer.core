
package com.filetransfer.client.scan.callback;

import com.filetransfer.client.scan.ScanCallBack;
import com.filetransfer.client.singlesend.SendCallBack;
import com.filetransfer.client.singlesend.SingleSendClient;
import com.filetransfer.client.state.TransferControl;
import com.filetransfer.config.FileClientConfig;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.util.ExceptionUtil;
import com.filetransfer.util.SendFileUtil;


/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class EdenScanCallBack extends ScanCallBack {

	
	public EdenScanCallBack(TransferControl transferControl)
	{
		super(transferControl);
		if(transferControl==null)
		{
			ExceptionUtil.throwException("EdenScanCallBack transferControl不能为空");
		}
		this.transferControl=transferControl;

	}

	private TransferControl transferControl;

	
	
	
	
	
	
	public void doNotify() {
		
		
		
	}

	public void nextStep(FileInfo fileInfo) {
		
	/*	SingleSendClient SingleTransferClient=new SingleSendClient(transferControl.getClientConfig(),sendCallBack);
		String wantFilePath=SendFileUtil.getPathFileName(transferControl.getClientConfig().getMonitorPath(),fileInfo.getFilePath());
		SingleTransferClient.sendFile(wantFilePath, fileInfo);*/
		transferControl.getBatchTransferClient().putFileInfo(fileInfo);
		
	}

}
