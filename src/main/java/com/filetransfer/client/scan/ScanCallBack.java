
package com.filetransfer.client.scan;

import java.io.File;

import com.filetransfer.cache.FileCacheFactory;
import com.filetransfer.client.state.TransferControl;
import com.filetransfer.config.FileClientConfig;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.eis.FileInfoState;
import com.filetransfer.util.ExceptionUtil;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public abstract class ScanCallBack {
	
	private TransferControl transferControl;
	
	public ScanCallBack(TransferControl transferControl)
	{
		if(transferControl==null)
		{
			ExceptionUtil.throwException("FileFolderScan的transferControl不能为null");
		}
		
		this.transferControl=transferControl;
	}
	
	/**
	 * 判断该文件是否要传送,要传送的为true，不传为false
	 * @return
	 */
	public boolean checkIsSend(File file)
	{
		boolean flag=false; //
		Integer fileState=FileCacheFactory.getFileInfoIdByFile(transferControl.getClientConfig().getCacheName(),file);
		if(fileState==null)
		{
			flag=true;
		}else{
			
			if(!FileInfoState.isSucessState(fileState))  
			{
				flag=true;
			}
			
		}
		return flag;
	}

	public abstract void doNotify();
	
	/**
	 * 下一步
	 */
	public abstract void nextStep(FileInfo fileInfo);
}
