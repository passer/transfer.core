
package com.filetransfer.client.scan;

import java.io.File;
import java.sql.Timestamp;
import java.util.LinkedList;

import com.filetransfer.cache.CacheManager;
import com.filetransfer.cache.FileCacheFactory;
import com.filetransfer.client.state.TransferControl;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.eis.FileInfoState;
import com.filetransfer.util.ExceptionUtil;
import com.filetransfer.util.FileInfoUtil;
import com.filetransfer.util.LogUtil;




/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileFolderScan {

	/**
	 * 是否本次传送超出了最大传送值
	 */
	private boolean isOutMaxSend=false;
	
	private TransferControl transferControl;

	
	
	public FileFolderScan(TransferControl transferControl)
	{
		if(transferControl==null)
		{
			ExceptionUtil.throwException("FileFolderScan的transferControl不能为null");
		}
		
		this.transferControl=transferControl;

	}
	
	
	public void startScan()
	{
		
		if(!transferControl.getTransferState().isTransferEnd())
		{
			LogUtil.echo("现在正在传输中");
			return;
		}
		
		if(transferControl.getClientFilePlugin()!=null)
		{
			transferControl.getClientFilePlugin().startScanBefore();
		}
		transferControl.getTransferState().setPutOver(false);
		transferControl.startTransfer();
		
		if(transferControl.getClientConfig().isMonitor())
		{
			fileScan(transferControl.getClientConfig().getMonitorPath());
		
		}else{
			
			LogUtil.err("clientConfig的isMonitor不是true,不能扫描文件夹监控！");
		}
		
		transferControl.getTransferState().setPutOver(true);
		System.out.println("本次扫描文件结束了！");
		if(transferControl.getTransferState().getTransferCount()==0||transferControl.getTransferState().getRemainCount()<=0) //没有文件要发送
		{
			transferControl.endTransfer();
		}
		
	
		
		//System.exit(0);
	}
	
	
	public void fileScan(String dirPath)
	{
		
		int num = 0;
		LinkedList<File> list = new LinkedList<File>();
		File dir = new File(dirPath);
		File files[] = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].isDirectory())
				list.add(files[i]);
			else {
				// System.out.println(file[i].getAbsolutePath());
				handSingleFile(files[i]);
				num++;
			}
		}
		File tmp;
		while (!list.isEmpty()) {
			tmp = (File) list.removeFirst();// 首个目录
			if (tmp.isDirectory()) {
				files = tmp.listFiles();
				if (files == null)
					continue;
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory())
						list.add(files[i]);// 目录则加入目录列表，关键
					else {
						handSingleFile(files[i]);
						num++;
					}
					if(isOutMaxSend)
					{
						break;
					}
					
				}
			} else {
				handSingleFile(tmp);
				num++;
			}
			
			if(isOutMaxSend)
			{
				break;
			}
		}
		
	}
	
	/**
	 * 处理扫描到的文件
	 * @param file
	 */
	private void handSingleFile(File file) {
	
		//boolean isCacheScan=true;
		
		if(transferControl.getScanCallBack().checkIsSend(file)) //判断是否要传送
		{
			boolean isReturn=false; //是否退出
			if(transferControl.getClientFilePlugin()!=null)  //拦截
			{
				isReturn=transferControl.getClientFilePlugin().handScanSingleFileBefore(file);
			}
			if(!isReturn)
			{
				getFileInfoAndPutList(file.getAbsolutePath());
				transferControl.getScanCallBack().doNotify();
			}
		

		}

	}
	

	/**
	 * 获取文件信息，放入队列
	 * 
	 * @param absolutePath
	 *            文件绝对路径
	 * @param isPutList
	 *            是否放入队列
	 */
	public FileInfo getFileInfoAndPutList(String absolutePath) {
		FileInfo fileInfo = new FileInfo();
		fileInfo.setFilePath(absolutePath);
		fileInfo.setState(0);
		fileInfo.setStateInfo(FileInfoState.getStateInfoByState(fileInfo.getState()));
		try {
			
			FileInfoUtil.putFileInfoPro(fileInfo,transferControl.getClientConfig().isIsMd5Check());
			transferControl.increaseCount();
			
			boolean isReturn=false; //是否退出
			if(transferControl.getClientFilePlugin()!=null)  //拦截
			{
				isReturn=transferControl.getClientFilePlugin().scanNextStep(fileInfo);
			}
			if(!isReturn)
			{
				transferControl.getScanCallBack().nextStep(fileInfo);
			}
			
			
			return fileInfo;
			
		} catch (Exception e) {

				LogUtil.err("文件扫描时错误了 " + fileInfo.getFilePath() + "错误！",e);

		}

		return null;

	}
	
	
	
	
	
}
