
package com.filetransfer.client.transitsend;

import com.filetransfer.client.singlesend.SingleSendClient;
import com.filetransfer.client.state.ServerReceiveControl;
import com.filetransfer.client.message.SendMessageClient;
import com.filetransfer.client.message.callback.TransitSendMessageCallBack;
import com.filetransfer.client.message.defaultadapter.EdenSendMessageAdapter;
import com.filetransfer.config.FileClientConfig;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.transport.util.FileInfoMessageUtil;
import com.filetransfer.util.LogUtil;

/**
 * 功能说明: 中转发送文件  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class TransitSendClient extends SingleSendClient {

	private ServerReceiveControl serverReceiveControl;
	
	public TransitSendClient(FileClientConfig clientConfig,ServerReceiveControl serverReceiveControl) {
		super(clientConfig);
		
		this.serverReceiveControl=serverReceiveControl;
	}
	

	@Override
	public void sucessTransferCallBack(FileInfoMessage fileInfoMessage) {
		super.sucessTransferCallBack(fileInfoMessage);
		
		LogUtil.echo(fileInfoMessage.getAbsolutePath()+"传输文件到目标主机成功！");
		if(serverReceiveControl.getServerFilePlugin()!=null) //拦截
		{
			serverReceiveControl.getServerFilePlugin().sucessFileToTarget(fileInfoMessage);
		}
		if(fileInfoMessage!=null)
		{
			fileInfoMessage.setState(3);
			
			sendFileStateMessage(fileInfoMessage);
		}
		
	}

	@Override
	public void finalFailTransferCallBack(FileInfoMessage fileInfoMessage) {
		super.finalFailTransferCallBack(fileInfoMessage);
		
		System.out.println(fileInfoMessage.getAbsolutePath()+"传输文件到目标主机失败！");	
		if(serverReceiveControl.getServerFilePlugin()!=null) //拦截
		{
			serverReceiveControl.getServerFilePlugin().failFileToTarget(fileInfoMessage);
		}
		if(fileInfoMessage!=null)
		{
			fileInfoMessage.setState(4);

			sendFileStateMessage(fileInfoMessage);
		}
	}

	
	private void sendFileStateMessage(FileInfoMessage fileInfoMessage)
	{
		FileInfoMessageUtil.changeReply2SendTo(fileInfoMessage, true);	
		SendMessageClient sendMessageClient=null;
		/*if(serverReceiveControl.getServerFilePlugin()!=null)
		{
			TransitSendMessageCallBack transitSendMessageCallBack=new TransitSendMessageCallBack(serverReceiveControl);
			EdenSendMessageAdapter edenSendMessageAdapter=new EdenSendMessageAdapter(transitSendMessageCallBack);
			
			sendMessageClient=new SendMessageClient(edenSendMessageAdapter);
		}else{
			sendMessageClient=new SendMessageClient();
		}*/
		sendMessageClient=new SendMessageClient(serverReceiveControl.getSendMessageAdapter());
		sendMessageClient.sendFileStateMessage(fileInfoMessage); //发送传输成功消息给原主机
	}
	
	
}
