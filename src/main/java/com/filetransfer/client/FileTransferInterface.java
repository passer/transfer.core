
package com.filetransfer.client;

import com.filetransfer.transport.command.FileInfoMessage;



/**
 * 功能说明: 文件发送接口 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public interface FileTransferInterface {

	/**
	 * 成功传输回调
	 * @param fileInfo
	 */
	public void sucessTransferCallBack(FileInfoMessage fileInfoMessage);
	
	/**
	 * 失败传输回调
	 * @param fileInfo
	 */
	public void failTransferCallBack(FileInfoMessage fileInfoMessage);
}
