
package com.filetransfer.client.batchtransfer;

import com.filetransfer.cache.FileCacheFactory;
import com.filetransfer.client.singlesend.SendCallBack;
import com.filetransfer.client.state.TransferControl;
import com.filetransfer.config.FileClientConfig;
import com.filetransfer.eis.FileInfoState;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.util.ExceptionUtil;
import com.filetransfer.util.LogUtil;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-29 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class EdenSendCallBack implements SendCallBack {

	private TransferControl transferControl;
	
	public EdenSendCallBack(TransferControl transferControl)
	{
		if(transferControl==null||transferControl.getClientConfig()==null)
		{
			ExceptionUtil.throwException("FileFolderScan的transferControl和ClientConfig不能为null");
		}
		
		this.transferControl=transferControl;
	}

	public void sucessSendCallBack(FileInfoMessage fileInfoMessage) {

		if(transferControl.getClientConfig().isMonitor())
		{
			transferControl.decreaseRemainCount();
			fileInfoMessage.setState(FileInfoState.TRANSIT_SUCCESS_STATE);
			FileCacheFactory.addFileInfoId(transferControl.getClientConfig().getCacheName(),fileInfoMessage.getFileId(), fileInfoMessage.getState());
		
			if(transferControl.getClientFilePlugin()!=null) //拦截
			{
				transferControl.getClientFilePlugin().sucessSendCallBack(fileInfoMessage);
			}
		}
		
		LogUtil.echo(fileInfoMessage.getWantRelativePath()+"传送成功！state为"+fileInfoMessage.getState());
	}

	public void failSendCallBack(FileInfoMessage fileInfoMessage) {
		if(transferControl.getClientConfig().isMonitor())
		{
			transferControl.decreaseRemainCount();
			fileInfoMessage.setState(FileInfoState.TRANSIT_FAIL_STATE);
			FileCacheFactory.addFileInfoId(transferControl.getClientConfig().getCacheName(),fileInfoMessage.getFileId(), fileInfoMessage.getState());
			
			if(transferControl.getClientFilePlugin()!=null) //拦截
			{
				transferControl.getClientFilePlugin().failSendCallBack(fileInfoMessage);
			}
		}
		LogUtil.echo(fileInfoMessage.getWantRelativePath()+"传送失败了！state为"+fileInfoMessage.getState());
	}

	
	
}
