
package com.filetransfer.client.batchtransfer;

import java.util.concurrent.LinkedBlockingQueue;

import com.filetransfer.client.state.TransferControl;
import com.filetransfer.config.FileClientConfig;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.util.ExceptionUtil;
import com.filetransfer.util.LogUtil;
import com.filetransfer.util.StringUtil;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-29 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileSendQueuePut {

	private LinkedBlockingQueue<FileInfo> fileQueue;
	private TransferControl transferControl;
	
	public FileSendQueuePut(LinkedBlockingQueue<FileInfo> fileQueue,TransferControl transferControl)
	{
		if(!StringUtil.isAllNotEmpty(fileQueue,transferControl)||transferControl.getTransferState()==null)
		{
			ExceptionUtil.throwException("fileQueue transferControl和 TransferState 都不能为null");
			
		}
		
		this.transferControl=transferControl;
		this.fileQueue=fileQueue;
		
	}
	
	
	public void putFileQueue(FileInfo fileInfo)
	{
		//System.out.println("放入了"+fileInfo.getFileName());
		try {
			fileQueue.put(fileInfo);
		} catch (InterruptedException e) {
		
			LogUtil.err("fileQueue.put失败", e);
		}
		
	}
	
	
}
