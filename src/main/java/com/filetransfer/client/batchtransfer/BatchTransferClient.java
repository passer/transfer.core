
package com.filetransfer.client.batchtransfer;

import java.util.concurrent.LinkedBlockingQueue;

import com.filetransfer.client.singlesend.SingleSendClient;
import com.filetransfer.client.state.TransferControl;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.util.ExceptionUtil;
import com.filetransfer.util.StringUtil;


/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-29 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class BatchTransferClient  {

	private LinkedBlockingQueue<FileInfo> fileQueue;
	private FileSendQueuePut fileSendQueuePut;
	
	private TransferControl transferControl;

	public BatchTransferClient(TransferControl transferControl)
	{
		if(!StringUtil.isAllNotEmpty(transferControl)||transferControl.getTransferState()==null)
		{
			ExceptionUtil.throwException("transferControl和 TransferState 都不能为null");
			
		}
		
		this.transferControl=transferControl;
		init();
	}
	
	private void init()
	{
		fileQueue=new LinkedBlockingQueue<FileInfo>(2);
		
		
		fileSendQueuePut=new FileSendQueuePut(fileQueue, transferControl);
		int sendThreadNum=2;
		for(int i=0;i<sendThreadNum;i++)
		{
			FileSendQueueGet fileSendQueueGet=new FileSendQueueGet(fileQueue,transferControl);
			new Thread(fileSendQueueGet).start();
		}
		
		
		
	}
	
	
	public void putFileInfo(FileInfo fileInfo)
	{
		
		fileSendQueuePut.putFileQueue(fileInfo);
		
	}

}
