
package com.filetransfer.client.batchtransfer;

import java.util.concurrent.LinkedBlockingQueue;

import com.filetransfer.client.singlesend.SingleSendClient;
import com.filetransfer.client.state.TransferControl;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.util.ExceptionUtil;
import com.filetransfer.util.LogUtil;
import com.filetransfer.util.SendFileUtil;
import com.filetransfer.util.StringUtil;

/**
 * 功能说明: 文件发送队列接收 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-29 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileSendQueueGet implements Runnable {

	private LinkedBlockingQueue<FileInfo> fileQueue;
	private TransferControl transferControl;
	
	public FileSendQueueGet(LinkedBlockingQueue<FileInfo> fileQueue,TransferControl transferControl)
	{
		if(!StringUtil.isAllNotEmpty(fileQueue,transferControl)||transferControl.getTransferState()==null)
		{
			ExceptionUtil.throwException("fileQueue transferControl和 TransferState 都不能为null");
			
		}
		
		this.transferControl=transferControl;
		this.fileQueue=fileQueue;
	}
	
	
	public void run() {

		
		/*while(!transferControl.getTransferState().isTransferEnd())
		{*/
			while(true)
			{
				FileInfo fileinfo=null;
				try {
					//if(transferControl.getTransferState().isPutOver())
					//{
					//	 fileinfo=fileQueue.poll();
					//}else{
					//LogUtil.echo("开始要取出了 ");
						 fileinfo=fileQueue.take();
					//}
					//LogUtil.echo("取出了"+fileinfo.getFileName());
					SingleSendClient singleTransferClient=new SingleSendClient(transferControl.getClientConfig(),transferControl.getSendCallBack());
					singleTransferClient.setTransferControl(transferControl);
					String wantFilePath=SendFileUtil.getPathFileName(transferControl.getClientConfig().getMonitorPath(),fileinfo.getFilePath());
					singleTransferClient.sendFile(wantFilePath, fileinfo);
				} catch (InterruptedException e) {
					LogUtil.err("取出数据有问题", e);
				}catch (Exception e) {
					LogUtil.err("发送失败", e);
				}
				
				
			}
			
			//LogUtil.echo("本次传输结束了！！");
		//}
		
		//LogUtil.echo("所有传输都结束了！！");
	}
	
	
	
}
