
package com.filetransfer.client.singlesend;

import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明: 发送文件后回调 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-29 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public interface SendCallBack {

	/**
	 * 成功传送
	 * @param fileInfoMessage
	 */
	public void sucessSendCallBack(FileInfoMessage fileInfoMessage);
	
	/**
	 * 失败发送
	 * @param fileInfoMessage
	 */
	public void failSendCallBack(FileInfoMessage fileInfoMessage);
}
