
package com.filetransfer.client.state;

import com.filetransfer.client.message.SendMessageAdapter;
import com.filetransfer.client.message.callback.TransitSendMessageCallBack;
import com.filetransfer.client.message.defaultadapter.EdenSendMessageAdapter;
import com.filetransfer.command.ReceiveServerPulse;
import com.filetransfer.config.FileServerConfig;
import com.filetransfer.plugin.ServerFilePlugin;
import com.filetransfer.server.fileserver.FileReceiveCallBack;
import com.filetransfer.server.fileserver.callback.EdenFileReceiveCallBack;
import com.filetransfer.server.fileserver.callback.TransitFileReceiveCallBack;
import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明: 文件接收控制类 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-30 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ServerReceiveControl {
	
	/**
	 * 是否初始化过
	 */
	private boolean isInited=false;

	private FileServerConfig fileServerConfig;
	/**
	 * 拦截器插件
	 */
	private ServerFilePlugin serverFilePlugin;
	
	/**
	 * 文件接收回调
	 */
	private FileReceiveCallBack fileReceiveCallBack;
	/**
	 * 中转时发给源主机消息的适配器
	 */
	private SendMessageAdapter sendMessageAdapter;
	
	public ServerReceiveControl(FileServerConfig fileServerConfig)
	{
		if(fileServerConfig==null)
		{
			throw new RuntimeException(ServerReceiveControl.class.getName()+" fileServerConfig不能为null ");
		}
		
		this.fileServerConfig=fileServerConfig;
		
	}
	
	
	public void init()
	{
		if(!isInited)
		{
			if(fileReceiveCallBack==null)
			{
				if(fileServerConfig.isTransitServer()) 
				{
					//中转接收回调
					fileReceiveCallBack=new TransitFileReceiveCallBack(this);
				}else{
					//不中转的回调
					fileReceiveCallBack=new EdenFileReceiveCallBack(this);
				}
				
				
			}
			
			if(sendMessageAdapter==null) 
			{
				if(serverFilePlugin!=null)  //有拦截器
				{
					TransitSendMessageCallBack transitSendMessageCallBack=new TransitSendMessageCallBack(this);
					sendMessageAdapter=new EdenSendMessageAdapter(transitSendMessageCallBack);
					
				}else{
					
					sendMessageAdapter=new EdenSendMessageAdapter();
				}
				
			}
			
			isInited=true;
		}
		
	}
	
	/**
	 * 增加拦截器组件
	 * @param serverFilePlugin
	 */
	public void addServerFilePlugin(ServerFilePlugin serverFilePlugin)
	{
		if(this.serverFilePlugin!=null)
		{
			
			this.serverFilePlugin=(ServerFilePlugin) this.serverFilePlugin.installPlugin(serverFilePlugin);
		}else{
			
			this.serverFilePlugin=serverFilePlugin;
		}
		
		
	}
	
	/**
	 * 成功接收文件的处理
	 */
	public void sucessReceiveFileCall(FileInfoMessage fileInfoMessage)
	{
		if(serverFilePlugin!=null)
		{
			serverFilePlugin.sucessReceiveFileCall(fileInfoMessage);
		}
		if(getFileReceiveCallBack()!=null)
		{
			getFileReceiveCallBack().sucessReceiveCall(fileInfoMessage);
			
		}
		
	}
	
	/**
	 * 接收文件失败的处理
	 * @param fileInfoMessage
	 */
	public void failReceiveFileCall(FileInfoMessage fileInfoMessage)
	{
		if(serverFilePlugin!=null)
		{
			serverFilePlugin.failReceiveFileCall(fileInfoMessage);
		}
		if(getFileReceiveCallBack()!=null)
		{
			getFileReceiveCallBack().failReceiveCall(fileInfoMessage);
			
		}
		
		
	}
	
	
	public FileServerConfig getFileServerConfig() {
		return fileServerConfig;
	}

	public void setFileServerConfig(FileServerConfig fileServerConfig) {
		this.fileServerConfig = fileServerConfig;
	}

	public ServerFilePlugin getServerFilePlugin() {
		return serverFilePlugin;
	}

/*	public void setServerFilePlugin(ServerFilePlugin serverFilePlugin) {
		this.serverFilePlugin = serverFilePlugin;
	}*/

	public FileReceiveCallBack getFileReceiveCallBack() {
		return fileReceiveCallBack;
	}

	public void setFileReceiveCallBack(FileReceiveCallBack fileReceiveCallBack) {
		this.fileReceiveCallBack = fileReceiveCallBack;
	}


	public SendMessageAdapter getSendMessageAdapter() {
		return sendMessageAdapter;
	}


	public void setSendMessageAdapter(SendMessageAdapter sendMessageAdapter) {
		this.sendMessageAdapter = sendMessageAdapter;
	}



	
}
