
package com.filetransfer.client.state;

import java.util.Date;

/**
 * 功能说明: 传输状态 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-29 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class TransferState {

	/**
	 * 本次传输真正结束
	 */
	private boolean isTransferEnd=true;
	
	/**
	 * 本次扫描是否完，但发送文件不一定结束
	 */
	private boolean isPutOver=false;
	
	/**
	 * 传输的文件剩余计数，为0则当前传输完成
	 */
	private int remainCount=0; 
	/**
	 * 传输的文件总数量
	 */
	private int transferCount=0; 
	
	/**
	 * 开始传输时间
	 */
	private Date startTransferTime;
	
	/**
	 * 传输结束时间
	 */
	private Date endTransferTime;
	
	
	/**
	 * 增加传输文件计数
	 */
	public void increaseTransferCount()
	{
		transferCount++;
	}
	/**
	 * 增加剩余传输数量计数
	 */
	public void increaseRemainCount()
	{
		remainCount++;
	}
	
	/**
	 * 减少剩余传输数量计数
	 */
	public void decreaseRemainCount()
	{
		remainCount--;
	}

	
	
	public boolean isTransferEnd() {
		return isTransferEnd;
	}
	public void setTransferEnd(boolean isTransferEnd) {
		this.isTransferEnd = isTransferEnd;
	}
	public boolean isPutOver() {
		return isPutOver;
	}
	public void setPutOver(boolean isPutOver) {
		this.isPutOver = isPutOver;
	}
	public int getRemainCount() {
		return remainCount;
	}
	public void setRemainCount(int remainCount) {
		this.remainCount = remainCount;
	}
	public int getTransferCount() {
		return transferCount;
	}
	public void setTransferCount(int transferCount) {
		this.transferCount = transferCount;
	}
	public Date getStartTransferTime() {
		return startTransferTime;
	}
	public void setStartTransferTime(Date startTransferTime) {
		this.startTransferTime = startTransferTime;
	}
	public Date getEndTransferTime() {
		return endTransferTime;
	}
	public void setEndTransferTime(Date endTransferTime) {
		this.endTransferTime = endTransferTime;
	}

	
}
