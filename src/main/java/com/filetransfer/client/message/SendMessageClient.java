
package com.filetransfer.client.message;

import com.filetransfer.client.message.callback.DemoSendMessageCallBack;
import com.filetransfer.client.message.defaultadapter.EdenSendMessageAdapter;
import com.filetransfer.client.message.defaultadapter.SendMessageHandle;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.global.ThreadPoolFactory;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.transport.util.FileInfoMessageUtil;




/**
 * 功能说明:  消息发送客户端,默认使用EdenSendMessageAdapter适配器<br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */

public class SendMessageClient {

	//private SendMessageCallBack sendMessageCallBack;
	
	private SendMessageAdapter sendMessageAdapter;
	/**
	 * 默认使用EdenSendMessageAdapter适配器
	 */
	public SendMessageClient()
	{
		
		sendMessageAdapter=new EdenSendMessageAdapter();
	}
	/**
	 * 使用其他适配器
	 * @param sendMessageAdapter
	 */
	public SendMessageClient(SendMessageAdapter sendMessageAdapter)
	{
		if(sendMessageAdapter==null)
		{
			throw new RuntimeException("sendMessageAdapter不能为null");
		}
		
		this.sendMessageAdapter=sendMessageAdapter;
	}
	
	/**
	 * 发送消息
	 * @param fileInfo
	 * @param sendToIp
	 * @param sendToPort
	 */
	public void sendFileStateMessage(FileInfo fileInfo,String sendToIp,Integer sendToPort)
	{
		sendMessageAdapter.sendFileStateMessage(fileInfo, sendToIp, sendToPort);
	      
	}
	/**
	 * 发送消息
	 * @param fileInfoMessage
	 */
	public void sendFileStateMessage(FileInfoMessage fileInfoMessage)
	{
		sendMessageAdapter.sendFileStateMessage(fileInfoMessage);
		
	}
	
	
	
	
	public SendMessageAdapter getSendMessageAdapter() {
		return sendMessageAdapter;
	}

	public void setSendMessageAdapter(SendMessageAdapter sendMessageAdapter) {
		this.sendMessageAdapter = sendMessageAdapter;
	}
	
	
	public SendMessageCallBack getSendMessageCallBack() {
		if(sendMessageAdapter!=null)
		{
			return sendMessageAdapter.getSendMessageCallBack();
		}else{
			throw new RuntimeException("sendMessageAdapter不能为null");
		}
		
		
	}

	public void setSendMessageCallBack(SendMessageCallBack sendMessageCallBack) {
		if(sendMessageAdapter!=null)
		{
			sendMessageAdapter.setSendMessageCallBack(sendMessageCallBack);
		}else{
			throw new RuntimeException("sendMessageAdapter不能为null");
		}
	}

	public static void main(String[] args) {
		FileInfoMessage fileInfoMessage=new FileInfoMessage();
		fileInfoMessage.setFileId("fileidtest");
		fileInfoMessage.setMd5("sddsdsdsdsfd");
		fileInfoMessage.setWantRelativePath("fff.txt");
		fileInfoMessage.setSendToIp("localhost");
		fileInfoMessage.setSendToPort(112);
		DemoSendMessageCallBack demoSendMessageCallBack=new DemoSendMessageCallBack();
		SendMessageClient sendMessageClient=new SendMessageClient();
		sendMessageClient.setSendMessageCallBack( demoSendMessageCallBack);
		
		sendMessageClient.sendFileStateMessage(fileInfoMessage);
	}
}
