
package com.filetransfer.client.message;

import com.filetransfer.eis.FileInfo;
import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public abstract class SendMessageAdapter {
	
	
	private SendMessageCallBack sendMessageCallBack;
	
	
	public SendMessageAdapter(SendMessageCallBack sendMessageCallBack)
	{
		this.setSendMessageCallBack(sendMessageCallBack);
	}
	
	public SendMessageAdapter()
	{
		
	}
	
	/**
	 * 发送文件信息消息
	 * @param fileInfo
	 * @param sendToIp
	 * @param sendToPort
	 */
	public abstract void sendFileStateMessage(FileInfo fileInfo,String sendToIp,Integer sendToPort);
	/**
	 * 发送文件信息消息
	 * @param fileInfoMessage
	 */
	public abstract void sendFileStateMessage(FileInfoMessage fileInfoMessage);

	public SendMessageCallBack getSendMessageCallBack() {
		return sendMessageCallBack;
	}

	public void setSendMessageCallBack(SendMessageCallBack sendMessageCallBack) {
		this.sendMessageCallBack = sendMessageCallBack;
	}
	
}
