
package com.filetransfer.client.message.callback;

import com.filetransfer.client.state.ServerReceiveControl;
import com.filetransfer.client.message.SendMessageCallBack;
import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明: 中转时的发送文件消息给源主机回调 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class TransitSendMessageCallBack implements SendMessageCallBack {

	private ServerReceiveControl serverReceiveControl;
	public TransitSendMessageCallBack(ServerReceiveControl serverReceiveControl)
	{
		this.serverReceiveControl=serverReceiveControl;
	}
	
	public void sucessMessageCallBack(FileInfoMessage fileInfoMessage) {
		System.out.println(fileInfoMessage.getWantRelativePath()+" 发送message TransitSendMessageCallBack callback成功！");
		if(serverReceiveControl.getServerFilePlugin()!=null)
		{
			serverReceiveControl.getServerFilePlugin().sucessMessageToSource(fileInfoMessage);
		}
	}

	public void failMessageCallBack(FileInfoMessage fileInfoMessage) {
		if(serverReceiveControl.getServerFilePlugin()!=null)
		{
			serverReceiveControl.getServerFilePlugin().failMessageToSource(fileInfoMessage);
		}
	}

}
