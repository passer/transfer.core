
package com.filetransfer.client.message.callback;

import com.filetransfer.client.message.SendMessageCallBack;
import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class DemoSendMessageCallBack implements SendMessageCallBack {

	public void sucessMessageCallBack(FileInfoMessage fileInfoMessage) {
		System.out.println(fileInfoMessage.getWantRelativePath()+" 发送message DemoSendMessageCallBack callback成功！");
	}

	public void failMessageCallBack(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		
	}

}
