
package com.filetransfer.client.message;

import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public interface SendMessageCallBack {
	

	/**
	 * 成功发送回调
	 * @param fileInfoMessage
	 */
	public void sucessMessageCallBack(FileInfoMessage fileInfoMessage);
	
	/**
	 *  失败发送回调,要注意fileInfoMessage可能为空
	 * @param fileInfoMessage
	 */
	public void failMessageCallBack(FileInfoMessage fileInfoMessage);

}
