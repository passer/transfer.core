
package com.filetransfer.client.message.defaultadapter;

import com.filetransfer.client.message.SendMessageAdapter;
import com.filetransfer.client.message.SendMessageCallBack;
import com.filetransfer.client.message.callback.TransitSendMessageCallBack;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.global.ThreadPoolFactory;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.transport.util.FileInfoMessageUtil;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-26 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class EdenSendMessageAdapter extends SendMessageAdapter {

	
	
	

	public EdenSendMessageAdapter() {
		super();
		
	}

	public EdenSendMessageAdapter(SendMessageCallBack sendMessageCallBack) {
		super(sendMessageCallBack);
		
	}

	@Override
	public void sendFileStateMessage(FileInfo fileInfo, String sendToIp,
			Integer sendToPort) {
		
		FileInfoMessage fileInfoMessage=FileInfoMessageUtil.fileInfoToFileInfoMessage(fileInfo,false);
		fileInfoMessage.setSendToIp(sendToIp);
		fileInfoMessage.setSendToPort(sendToPort);
		ThreadPoolFactory.getExecutorService().execute(new SendMessageHandle(fileInfoMessage,this.getSendMessageCallBack()
				));  
		
	}

	@Override
	public void sendFileStateMessage(FileInfoMessage fileInfoMessage) {

		ThreadPoolFactory.getExecutorService().execute(new SendMessageHandle(fileInfoMessage,this.getSendMessageCallBack()));  
		
	}
	
	
	

	
	
}
