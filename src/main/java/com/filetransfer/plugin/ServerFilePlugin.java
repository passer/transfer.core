
package com.filetransfer.plugin;


import java.util.ArrayList;
import java.util.List;

import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明: 文件服务端插件拦截  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-30 <br>
 * ======== ====== ============================================ <br>
 * 
 */

public class ServerFilePlugin extends FilePlugin {

	
	//public FilePlugin preFilePlugin;
	
/*	@Override
	public ServerFilePlugin installPlugin(FilePlugin preFilePlugin) {
		return null;
	}*/
	

	/**
	 * 接收到文件Message信息，还没接收文件时,如果要拦截停止，向客户端返回消息，把isReturn设为true，fileInfoMessage的replyInfo写要返回的内容
	 * @param fileInfoMessage
	 * @return 是否拦截使停止,返回消息
	 */
	public boolean receiveFileInfoMessageBefore(FileInfoMessage fileInfoMessage)
	{
		//fileInfoMessage=invokePrePluginMethod("receiveFileInfoMessageBefore", fileInfoMessage);
		boolean isReturn=false;
		if(preFilePlugin!=null)
		{
			isReturn=((ServerFilePlugin)preFilePlugin).receiveFileInfoMessageBefore(fileInfoMessage);
		}
		
		return isReturn;
	}
	
	/**
	 * 成功接收到文件时拦截
	 * @param fileInfoMessage
	 */
	public FileInfoMessage sucessReceiveFileCall(FileInfoMessage fileInfoMessage)
	{
		fileInfoMessage=invokePrePluginMethod("sucessReceiveFileCall", fileInfoMessage);
		
		return fileInfoMessage;
	}
	/**
	 * 接收到文件失败拦截
	 * @param fileInfoMessage
	 */
	public FileInfoMessage failReceiveFileCall(FileInfoMessage fileInfoMessage)
	{
		fileInfoMessage=invokePrePluginMethod("failReceiveFileCall", fileInfoMessage);
		
		return fileInfoMessage;
	}
	
	
	
	
	/**
	 * 当前作为中转主机时，要开始发送文件到目标时拦截
	 * @param fileInfoMessage
	 */
	public FileInfoMessage sendFileToTagertBefore(FileInfoMessage fileInfoMessage)
	{
		//invokePrePluginMethod("sendFileToTagertBefore", fileInfoMessage);
		/*boolean isReturn=false;
		if(preFilePlugin!=null)
		{
			isReturn=((ServerFilePlugin)preFilePlugin).sendFileToTagertBefore(fileInfoMessage);
		}*/
		
		return fileInfoMessage;
	}
	
	
	/**
	 * 当前作为中转主机时，成功把文件传送至目标
	 * @param fileInfoMessage
	 */
	public FileInfoMessage sucessFileToTarget(FileInfoMessage fileInfoMessage)
	{
		fileInfoMessage=invokePrePluginMethod("sucessFileToTarget", fileInfoMessage);
		
		return fileInfoMessage;
	}
	
	/**
	 * 当前作为中转主机时，把文件传送至目标失败
	 * @param fileInfoMessage
	 */
	public FileInfoMessage failFileToTarget(FileInfoMessage fileInfoMessage)
	{
		fileInfoMessage=invokePrePluginMethod("failFileToTarget", fileInfoMessage);
		
		return fileInfoMessage;
	}
	
	
	/**
	 * 当前作为中转主机时，成功发送给源主机的拦截
	 * @param fileInfoMessage
	 */
	public FileInfoMessage sucessMessageToSource(FileInfoMessage fileInfoMessage)
	{
		fileInfoMessage=invokePrePluginMethod("sucessMessageToSource", fileInfoMessage);
		return fileInfoMessage;
	}
	
	/**
	 * 当前作为中转主机时，发送给源主机失败的拦截
	 * @param fileInfoMessage
	 */
	public FileInfoMessage failMessageToSource(FileInfoMessage fileInfoMessage)
	{
		fileInfoMessage=invokePrePluginMethod("failMessageToSource", fileInfoMessage);
		
		return fileInfoMessage;
	}
	
	
	public static void main(String[] args) {
		
		List<FilePlugin> list=new ArrayList<FilePlugin>();
		ServerFilePlugin serverFilePlugin=new ServerFilePlugin();
		ServerFilePlugin serverFilePlugin2=new ServerFilePlugin();
		list.add(serverFilePlugin);
		list.add(serverFilePlugin2);
		
		
		 FilePlugin filePlugin=null;
		for(int i=0;i<list.size();i++)
		{
			
			filePlugin=filePlugin.installPlugin(list.get(i));
		}
		
		
		
		
		
	}



/*	@Override
	public ServerFilePlugin queryForObject(FilePlugin preFilePlugin) {
		// TODO Auto-generated method stub
		return null;
	}*/



	
}
