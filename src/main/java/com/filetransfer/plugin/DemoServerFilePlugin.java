
package com.filetransfer.plugin;

import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-10-8 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class DemoServerFilePlugin extends ServerFilePlugin {

	@Override
	public boolean receiveFileInfoMessageBefore(
			FileInfoMessage fileInfoMessage) {
		System.out.println("DemoServerFilePlugin receiveFileInfoMessageBefore拦截");
		
		//super.receiveFileInfoMessageBefore(fileInfoMessage);
		//fileInfoMessage.setReplyInfo("has no permit");
		return super.receiveFileInfoMessageBefore(fileInfoMessage);
	}

	@Override
	public FileInfoMessage sucessReceiveFileCall(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoServerFilePlugin sucessReceiveFileCall拦截");
		return super.sucessReceiveFileCall(fileInfoMessage);
	}

	@Override
	public FileInfoMessage failReceiveFileCall(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoServerFilePlugin failReceiveFileCall拦截");
		return super.failReceiveFileCall(fileInfoMessage);
	}

	@Override
	public FileInfoMessage sendFileToTagertBefore(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoServerFilePlugin sendFileToTagertBefore拦截");
		return super.sendFileToTagertBefore(fileInfoMessage);
	}

	@Override
	public FileInfoMessage sucessFileToTarget(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoServerFilePlugin sucessFileToTarget拦截");
		return super.sucessFileToTarget(fileInfoMessage);
	}

	@Override
	public FileInfoMessage failFileToTarget(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoServerFilePlugin failFileToTarget拦截");
		return super.failFileToTarget(fileInfoMessage);
	}

	@Override
	public FileInfoMessage sucessMessageToSource(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoServerFilePlugin sucessMessageToSource拦截");
		return super.sucessMessageToSource(fileInfoMessage);
	}

	@Override
	public FileInfoMessage failMessageToSource(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoServerFilePlugin failMessageToSource拦截");
		return super.failMessageToSource(fileInfoMessage);
	}

	
	
	
}
