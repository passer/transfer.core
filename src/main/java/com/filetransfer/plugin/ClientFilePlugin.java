
package com.filetransfer.plugin;

import java.io.File;

import com.filetransfer.eis.FileInfo;
import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-10-9 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ClientFilePlugin  extends FilePlugin {

	
	private ClientFilePlugin getPreClientFilePlugin()
	{
		if(preFilePlugin!=null)
		{
			return (ClientFilePlugin)preFilePlugin;
		}
		return null;
	}
	
	/**
	 * 开始扫描时拦截
	 */
	public void startScanBefore()
	{
		if(preFilePlugin!=null)
		{
			getPreClientFilePlugin().startScanBefore();
		}
		
		
	}
	
	
	/**
	 * 结束本次扫描传输时拦截
	 */
	public void endTransferAfter()
	{
		if(preFilePlugin!=null)
		{
			getPreClientFilePlugin().endTransferAfter();
		}
	}
	
	
	/**
	 *  扫描到文件后要处理时拦截
	 * @param file
	 * @return 是否继续
	 */
	public boolean handScanSingleFileBefore(File file)
	{
		boolean isReturn=false;
		if(preFilePlugin!=null)
		{
			isReturn=getPreClientFilePlugin().handScanSingleFileBefore(file);
		}
		return isReturn;
	}
	
	/**
	 * 扫描到文件后下一步,注意强制退出了要把计数减一：用 transferControl.decreaseRemainCount(); ，否则会出错。
	 * @param fileInfo
	 * @return 是否退出，true则会退出
	 */
	public boolean scanNextStep(FileInfo fileInfo)
	{
		boolean isReturn=false;
		if(preFilePlugin!=null)
		{
			isReturn=getPreClientFilePlugin().scanNextStep(fileInfo);
		}
		
		return isReturn;
		
	}
	
	/**
	 * 开始发送文件时拦截
	 * 
	 */
	public void sendFileBefore(FileInfoMessage fileInfoMessage)
	{
		if(preFilePlugin!=null)
		{
			getPreClientFilePlugin().sendFileBefore(fileInfoMessage);
		}
		
		
	}
	
	/**
	 * 成功发送文件到中转主机返回
	 * @param fileInfoMessage
	 */
	public void sucessSendCallBack(FileInfoMessage fileInfoMessage)
	{
		if(preFilePlugin!=null)
		{
			getPreClientFilePlugin().sucessSendCallBack(fileInfoMessage);
		}
	}
	
	/**
	 * 发送文件到中转主机失败
	 * @param fileInfoMessage ，发送失败消息在 fileInfoMessage.getReplyInfo()
	 */
	public void failSendCallBack(FileInfoMessage fileInfoMessage)
	{
		if(preFilePlugin!=null)
		{
			getPreClientFilePlugin().failSendCallBack(fileInfoMessage);
		}
	}
	
	/**
	 * 接收文件消息
	 * @param fileInfoMessage
	 */
	public void receiveFileMessage(FileInfoMessage fileInfoMessage)
	{
		if(preFilePlugin!=null)
		{
			getPreClientFilePlugin().receiveFileMessage(fileInfoMessage);
		}
	}
	
}
