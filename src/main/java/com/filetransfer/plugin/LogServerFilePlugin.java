
package com.filetransfer.plugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.filetransfer.eis.FileInfoState;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.util.JsonUtils;




/**
 * 功能说明:  <br>
 * 系统版本: v1.0 <br>
 * 开发人员: hanxuetong@rongji.com <br>
 * 开发时间: 2014-10-8 <br>
 * 审核人员:  <br>
 * 相关文档:  <br>
 * 修改记录:  <br>
 * 修改日期 修改人员 修改说明  <br>
 * ======== ====== ============================================ <br>
 * 
 */

public class LogServerFilePlugin extends ServerFilePlugin {


	private static final Logger logger = LoggerFactory.getLogger(LogServerFilePlugin.class);
	
	private void updateFileInfo(FileInfoMessage fileInfoMessage,int state,String stateInfo)
	{
		System.out.println(JsonUtils.toJson(fileInfoMessage));
		logger.info("update fileInfo:{},stateInfo:{}",JsonUtils.toJson(fileInfoMessage),stateInfo);
	}
	
	@Override
	public FileInfoMessage sucessReceiveFileCall(FileInfoMessage fileInfoMessage) {

		
		updateFileInfo(fileInfoMessage,FileInfoState.TRANSIT_SUCCESS_STATE,"中转主机接收成功");
		
		return super.sucessReceiveFileCall(fileInfoMessage);
	}

	@Override
	public FileInfoMessage failReceiveFileCall(FileInfoMessage fileInfoMessage) {

		updateFileInfo(fileInfoMessage,FileInfoState.TRANSIT_FAIL_STATE,"中转主机接收失败");
		return super.failReceiveFileCall(fileInfoMessage);
	}

	@Override
	public FileInfoMessage sucessFileToTarget(FileInfoMessage fileInfoMessage) {

		/*FileInfo fileInfo=FileInfoMessageUtil.fileInfoMessageToFileInfo(fileInfoMessage);
		fileInfo.setState(FileInfoState.Target_SUCCESSL_WITHTRANSIT_STATE);
		fileInfo.setStateInfo("传送到目标成功");
		fileInfoService.updateState(fileInfo);*/
		logger.info("update fileInfo:{},stateInfo:{}",JsonUtils.toJson(fileInfoMessage),"传送到目标成功");
		return super.sucessFileToTarget(fileInfoMessage);
	}

	@Override
	public FileInfoMessage failFileToTarget(FileInfoMessage fileInfoMessage) {

	/*	FileInfo fileInfo=FileInfoMessageUtil.fileInfoMessageToFileInfo(fileInfoMessage);
		fileInfo.setState(FileInfoState.Target_FAIL_WITHTRANSIT_STATE);
		fileInfo.setStateInfo("传送到目标失败");
		fileInfoService.updateState(fileInfo);*/
		logger.info("update fileInfo:{},stateInfo:{}",JsonUtils.toJson(fileInfoMessage),"传送到目标失败");
		
		return super.failFileToTarget(fileInfoMessage);
	}

	@Override
	public FileInfoMessage sucessMessageToSource(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		return super.sucessMessageToSource(fileInfoMessage);
	}

	@Override
	public FileInfoMessage failMessageToSource(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		return super.failMessageToSource(fileInfoMessage);
	}

	
	
	
}
