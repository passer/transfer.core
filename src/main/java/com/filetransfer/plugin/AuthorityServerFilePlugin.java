
package com.filetransfer.plugin;

import com.filetransfer.client.state.ServerReceiveControl;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.util.AuthorityUtil;
import com.filetransfer.util.LogUtil;
import com.filetransfer.util.StringUtil;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-10-10 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class AuthorityServerFilePlugin extends ServerFilePlugin {

	private ServerReceiveControl serverReceiveControl;
	
	public AuthorityServerFilePlugin( ServerReceiveControl serverReceiveControl)
	{
		this.serverReceiveControl=serverReceiveControl;
	}
	
	@Override
	public boolean receiveFileInfoMessageBefore(FileInfoMessage fileInfoMessage) {

		//进行auth验证
		if(serverReceiveControl.getFileServerConfig().isAuthorityCheck())
		{
			String auth=null;
			try {
				auth = fileInfoMessage.getStringProperty("auth");
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(StringUtil.isAllNotEmpty(auth))
			{
				String key=serverReceiveControl.getFileServerConfig().getAuthorityKey();
				String countAuth=AuthorityUtil.getAuthorityCode(key, fileInfoMessage.getFileId(), fileInfoMessage.getStartSendLongTime());
				if(auth.equals(countAuth)) //验证通过
				{
					LogUtil.echo("auth:"+auth+"验证成功！");
					return false;
				}else{
					
					fileInfoMessage.setReplyInfo("has no permit,auth error ");
					return true;
				}
				
			}else{ //没有权限
				
				fileInfoMessage.setReplyInfo("has no permit,auth can not null");
				return true;
			}
			
		}
		
		return super.receiveFileInfoMessageBefore(fileInfoMessage);
	}

	
	
}
