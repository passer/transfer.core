
package com.filetransfer.plugin;

import java.io.File;

import com.filetransfer.cache.FileCacheFactory;
import com.filetransfer.client.state.TransferControl;
import com.filetransfer.eis.FileInfo;
import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-10-9 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class DemoClientFilePlugin extends ClientFilePlugin {

	private TransferControl transferControl;
	public DemoClientFilePlugin(TransferControl transferControl)
	{
		this.transferControl=transferControl;
	}
	
	@Override
	public void startScanBefore() {
		// TODO Auto-generated method stub
		super.startScanBefore();
		
		System.out.println("DemoClientFilePlugin 开始扫描了  ");
		
	}

	@Override
	public void endTransferAfter() {
		// TODO Auto-generated method stub
		super.endTransferAfter();
		
		System.out.println("DemoClientFilePlugin 结束扫描文件  ");
	}

	@Override
	public boolean handScanSingleFileBefore(File file) {
		// TODO Auto-generated method stub
		
		System.out.println("DemoClientFilePlugin 处理单文件拦截  ");
		return super.handScanSingleFileBefore(file);
	}

	@Override
	public boolean scanNextStep(FileInfo fileInfo) {
		// TODO Auto-generated method stub
		
		System.out.println("DemoClientFilePlugin 扫描进入下一步拦截  ");
		transferControl.decreaseRemainCount(); 
	
		FileCacheFactory.addFileInfoId(transferControl.getClientConfig().getCacheName(),fileInfo.getId(), fileInfo.getState());
		System.out.println("DemoClientFilePlugin 拦截住了 ");
		return true;
		//return super.scanNextStep(fileInfo);
	}

	@Override
	public void sendFileBefore(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoClientFilePlugin 开始要发送文件拦截  ");
		super.sendFileBefore(fileInfoMessage);
	}

	@Override
	public void sucessSendCallBack(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		
		System.out.println("DemoClientFilePlugin 成功发送文件拦截  ");
		super.sucessSendCallBack(fileInfoMessage);
	}

	@Override
	public void failSendCallBack(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		
		System.out.println("DemoClientFilePlugin 发送文件失败 拦截  ");
		super.failSendCallBack(fileInfoMessage);
	}

	@Override
	public void receiveFileMessage(FileInfoMessage fileInfoMessage) {
		// TODO Auto-generated method stub
		System.out.println("DemoClientFilePlugin receiveFileMessage 拦截  ");
		super.receiveFileMessage(fileInfoMessage);
	}

	
	
	
}
