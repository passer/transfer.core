
package com.filetransfer.plugin;

import com.filetransfer.client.state.TransferControl;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.util.AuthorityUtil;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-10-10 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class AuthorityClientFilePlugin extends ClientFilePlugin {

	private TransferControl transferControl;
	public AuthorityClientFilePlugin(TransferControl transferControl)
	{
		this.transferControl=transferControl;
	}
	
	@Override
	public void sendFileBefore(FileInfoMessage fileInfoMessage) {
		//是否auth验证
		if(transferControl.getClientConfig().isAuthorityCheck())
		{
			String key=transferControl.getClientConfig().getAuthorityKey();
			String auth=AuthorityUtil.getAuthorityCode(key, fileInfoMessage.getFileId(), fileInfoMessage.getStartSendLongTime());
		
			try {
				fileInfoMessage.setStringProperty("auth", auth); //设置auth属性
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		super.sendFileBefore(fileInfoMessage);
	}

	
	
}
