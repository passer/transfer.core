
package com.filetransfer.plugin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-30 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public abstract class FilePlugin {

	/**
	 * 前一个Plugin
	 */
	public FilePlugin preFilePlugin;
	
	
	public FilePlugin installPlugin(FilePlugin preFilePlugin)
	{
		this.preFilePlugin=preFilePlugin;
		return this;
	}
	/**
	 * 执行前一个FilePlugin的方法
	 * @param methodName
	 * @param fileInfoMessage
	 */
	public FileInfoMessage invokePrePluginMethod(String methodName,FileInfoMessage fileInfoMessage)
	{
		
		if(preFilePlugin!=null)
		{
			try {
				  
				Method m =  preFilePlugin.getClass().getMethod(methodName,FileInfoMessage.class);
				fileInfoMessage=(FileInfoMessage)m.invoke(preFilePlugin, fileInfoMessage);
				return fileInfoMessage;
			  } catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}  
		}
		return null;
	}
	
	
//	public abstract <T extends FilePlugin> FilePlugin installPlugin(FilePlugin preFilePlugin);
	
	
}
