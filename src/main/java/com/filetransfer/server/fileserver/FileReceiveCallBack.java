
package com.filetransfer.server.fileserver;

import com.filetransfer.client.state.ServerReceiveControl;
import com.filetransfer.transport.command.FileInfoMessage;



/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */

public abstract class FileReceiveCallBack {

	protected ServerReceiveControl serverReceiveControl;
	
	public FileReceiveCallBack(ServerReceiveControl serverReceiveControl)
	{
		this.serverReceiveControl=serverReceiveControl;
	}
	
	public abstract void sucessReceiveCall(FileInfoMessage receiveMessage);
	
	
	public abstract void failReceiveCall(FileInfoMessage receiveMessage);
	
	
	
	
}
