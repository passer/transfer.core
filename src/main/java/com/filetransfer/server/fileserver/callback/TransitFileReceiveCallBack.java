
package com.filetransfer.server.fileserver.callback;

import java.io.IOException;

import com.filetransfer.client.state.ServerReceiveControl;
import com.filetransfer.client.transitsend.TransitSendClient;
import com.filetransfer.config.FileClientConfig;
import com.filetransfer.server.fileserver.FileReceiveCallBack;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.transport.util.FileInfoMessageUtil;
import com.filetransfer.transport.util.XStreamWireFormat;
/**
 * 功能说明: 当前作为中转服务器时回调  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-26 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class TransitFileReceiveCallBack extends FileReceiveCallBack {


	/**
	 * 中转发送给目标主机是的配置
	 */
	private FileClientConfig clientConfig=new FileClientConfig(false, true, false);
/*	*//**
	 * 服务器配置时
	 *//*
	private FileServerConfig fileServerConfig;*/
	
	
	public TransitFileReceiveCallBack(ServerReceiveControl serverReceiveControl) {
		super(serverReceiveControl);
		// TODO Auto-generated constructor stub
	}


	
	
	
	public void sucessReceiveCall(FileInfoMessage receiveMessage) {

		FileInfoMessage newReceiveMessage=receiveMessage.copyto(); //复制
		FileInfoMessageUtil.changeTarget2SendTo(newReceiveMessage, true); //进行改变SendTo
		newReceiveMessage.setTransit(false); //不进行中转
		// TODO Auto-generated constructor stub
		/*//要看下的
		try {
			System.out.println(XStreamWireFormat.marshalText(newReceiveMessage));
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		TransitSendClient transitTransferClient=new TransitSendClient(clientConfig,serverReceiveControl);
		if(serverReceiveControl.getServerFilePlugin()!=null) //拦截
		{
			serverReceiveControl.getServerFilePlugin().sendFileToTagertBefore(newReceiveMessage);
		}
		transitTransferClient.sendFile(newReceiveMessage);
		
	}

	public void failReceiveCall(FileInfoMessage receiveMessage) {

		
	}

/*	public FileServerConfig getFileServerConfig() {
		return fileServerConfig;
	}

	public void setFileServerConfig(FileServerConfig fileServerConfig) {
		this.fileServerConfig = fileServerConfig;
	}*/



}
