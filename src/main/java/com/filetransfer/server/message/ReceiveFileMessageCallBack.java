
package com.filetransfer.server.message;

import com.filetransfer.transport.command.FileInfoMessage;



/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public interface ReceiveFileMessageCallBack {

	public void sucessReceiveCall(FileInfoMessage fileInfoMessage);
	
	public void failReceiveCall(FileInfoMessage fileInfoMessage);
	
}
