
package com.filetransfer.server.message;

import com.filetransfer.config.ReceiveFileMessageConfig;
import com.filetransfer.server.message.callback.EdenReceiveFileMessageCallBack;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ReceiveFileMessagePulse extends Thread{

	private ReceiveFileMessageConfig receiveFileMessageConfig;
	
	public ReceiveFileMessagePulse(ReceiveFileMessageConfig receiveFileMessageConfig)
	{
		this.receiveFileMessageConfig=receiveFileMessageConfig;
		
	}
	@Override
	public void run() {

		
		try {
			
			ReceiveFileMessageServer receiveFileMessageServer=new ReceiveFileMessageServer(receiveFileMessageConfig);
			receiveFileMessageServer.service();
			System.out.println("初始化消息接收服务器成功！");
		} catch (Exception e) {
			//log.error("初始化文件接收服务器失败!!!");
			
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args) {
		EdenReceiveFileMessageCallBack edenReceiveFileMessageCallBack=new EdenReceiveFileMessageCallBack(null);
		ReceiveFileMessageConfig receiveFileMessageConfig=new ReceiveFileMessageConfig(112, edenReceiveFileMessageCallBack);
		ReceiveFileMessagePulse receiveFileMessagePulse=new ReceiveFileMessagePulse(receiveFileMessageConfig);
		receiveFileMessagePulse.start();
	}
	
}
