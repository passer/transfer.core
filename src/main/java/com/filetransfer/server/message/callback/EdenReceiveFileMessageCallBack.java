
package com.filetransfer.server.message.callback;

import com.filetransfer.client.state.TransferControl;
import com.filetransfer.server.message.ReceiveFileMessageCallBack;
import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class EdenReceiveFileMessageCallBack implements ReceiveFileMessageCallBack{

	private TransferControl transferControl; //可能会null
	
	public EdenReceiveFileMessageCallBack(TransferControl transferControl)
	{
		this.transferControl=transferControl;
	}
	
	public void sucessReceiveCall(FileInfoMessage fileInfoMessage) {
		
		if(transferControl!=null&&transferControl.getClientFilePlugin()!=null)  //拦截
		{
			transferControl.getClientFilePlugin().receiveFileMessage(fileInfoMessage);
		}
		
		System.out.println("EdenReceiveFileMessageCallBack "+fileInfoMessage.getFileId()+" "+fileInfoMessage.getWantRelativePath()+" 的状态为 "+fileInfoMessage.getState());
	}

	public void failReceiveCall(FileInfoMessage fileInfoMessage) {

		System.out.println("EdenReceiveFileMessageCallBack 消息发送失败"+fileInfoMessage.getFileId()+" "+fileInfoMessage.getWantRelativePath()+" 的状态为 "+fileInfoMessage.getState());
		
	}

	
	
}
