
package com.filetransfer.server.message;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

import com.filetransfer.config.ReceiveFileMessageConfig;
import com.filetransfer.transport.command.FileInfoMessage;
import com.filetransfer.transport.util.XStreamWireFormat;
import com.filetransfer.util.LogUtil;



/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ReceiveFileMessageHandle implements Runnable{

	private Socket sock;
	
	private ReceiveFileMessageConfig receiveFileMessageConfig;
	
	ReceiveFileMessageHandle(Socket sock,ReceiveFileMessageConfig receiveFileMessageConfig) throws IOException 
	{
		this.sock = sock;
		this.receiveFileMessageConfig=receiveFileMessageConfig;
		
	}
	
	public void run() {

		String ip = sock.getInetAddress().getHostAddress(); // 获取客户端ip
		System.out.println("从客户端ip : " + ip+"接收文件状态信息 ");
		DataInputStream sockIn=null;
		
		try {
			sockIn= new DataInputStream(new BufferedInputStream(sock.getInputStream()));
			
			String messageXml=sockIn.readUTF();		
			FileInfoMessage fileInfoMessage=XStreamWireFormat.unmarshalText(messageXml,FileInfoMessage.class);
			fileInfoMessage.setFromIp(ip);
			/*String id=sockIn.readUTF();
			String md5=sockIn.readUTF();
			int state=sockIn.readInt();
			
			FileInfo fileInfo=new FileInfo();
			fileInfo.setId(id);
			fileInfo.setMd5(md5);
			fileInfo.setState(state);
			*/
			//SystemApplication.getInstance().getReceiveFileMessageCallBack().sucessReceiveCall(fileInfoMessage);
			
			if(receiveFileMessageConfig.getReceiveFileMessageCallBack()!=null)
			{
				receiveFileMessageConfig.getReceiveFileMessageCallBack().sucessReceiveCall(fileInfoMessage);
			}
		} catch (IOException e) {
			
			//e.printStackTrace();
			LogUtil.err(e.getMessage()+"文件信息接收失败！",e);
			if(receiveFileMessageConfig.getReceiveFileMessageCallBack()!=null)
			{
				receiveFileMessageConfig.getReceiveFileMessageCallBack().failReceiveCall(null);
			}
		}finally{
			
			try {
				sockIn.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			
			try {
				sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

	
	
	
}
