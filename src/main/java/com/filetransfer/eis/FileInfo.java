
package com.filetransfer.eis;

import java.sql.Timestamp;



/**
 * 功能说明:  文件信息<br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-18 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileInfo {

	private String id;
	private Timestamp createDttm;	//数据创建时间
	private String fileName;
	private String fileSize;
	private String md5;
	private Timestamp lastTransferTime;  //
	private String filePath;
	private Long lastModified; //文件的最近修改时间
	
	private Integer state;  //状态  0 开始传输  1 B服务器 传输成功  2 B服务器传输失败   3 C服务器成功  4 C服务器失败
	private String stateInfo;  //状态中文信息
	private String transferId;
	private String remark;
	private Integer dataValid;		//数据有效性：0-无效；1-有效
	
	private String toHostString;  //怎样传送的路径字符串

	private String fromIp;  //发送文件主机ip
	
	private String targetIp; //传送目标主机ip
	private Integer targetPort; //传送目标主机端口
	
	
	public String getFileSizeFmt()
	{
		if(fileSize!=null)
		{
			long size=Long.parseLong(fileSize);
			if(size==0)
				size=1;
			if(size<1024)
			{
				return (int)size+"B";
			}
			else if(size<1024*1024)
			{
				return (int)size/1024+"KB";
			}else{
				
				return (int)size/1024/1024+"M";
			}
			
		}
		
		return "0KB";
	}
	

	
	
	public Timestamp getCreateDttm() {
		return createDttm;
	}
	public void setCreateDttm(Timestamp createDttm) {
		this.createDttm = createDttm;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMd5() {
		return md5;
	}
	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getStateInfo() {
		return stateInfo;
	}
	public void setStateInfo(String stateInfo) {
		this.stateInfo = stateInfo;
	}
	public String getTransferId() {
		return transferId;
	}
	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getDataValid() {
		return dataValid;
	}
	public void setDataValid(Integer dataValid) {
		this.dataValid = dataValid;
	}
	public Timestamp getLastTransferTime() {
		return lastTransferTime;
	}
	public void setLastTransferTime(Timestamp lastTransferTime) {
		this.lastTransferTime = lastTransferTime;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	/*public static String creatId()
	{
		String randId=System.currentTimeMillis()+"#"+(int)(Math.random()*100000000);
		System.out.println(randId);
		return randId;
	}*/



	public String getToHostString() {
		return toHostString;
	}

	public void setToHostString(String toHostString) {
		this.toHostString = toHostString;
	}

	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}




	public String getFromIp() {
		return fromIp;
	}




	public void setFromIp(String fromIp) {
		this.fromIp = fromIp;
	}




	public String getTargetIp() {
		return targetIp;
	}




	public void setTargetIp(String targetIp) {
		this.targetIp = targetIp;
	}




	public Integer getTargetPort() {
		return targetPort;
	}




	public void setTargetPort(Integer targetPort) {
		this.targetPort = targetPort;
	}


	
	
	
}
