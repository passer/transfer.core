
package com.filetransfer.eis;
/**
 * 功能说明: 文件传送状态 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-29 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileInfoState {

	/**
	 * 扫描到
	 */
	public static final Integer SCAN_STATE=0;
	/**
	 * 中转主机传送成功
	 */
	public static final  Integer TRANSIT_SUCCESS_STATE=1;
	/**
	 * 中转主机传送失败
	 */
	public static final  Integer TRANSIT_FAIL_STATE=2;
	
	/**
	 * 进行中转的目标传送成功状态
	 */
	public static final  Integer Target_SUCCESSL_WITHTRANSIT_STATE=3;
	
	/**
	 * 进行中转的目标传送失败状态
	 */
	public static final  Integer Target_FAIL_WITHTRANSIT_STATE=4;
	
	/**
	 * 不进行中转的目标传送成功状态
	 */
	public static final  Integer Target_SUCCESSL_NO_RANSIT_STATE=1;
	
	/**
	 * 不进行中转的目标传送失败状态
	 */
	public static final  Integer Target_FAIL_NO_TRANSIT_STATE=2;
	
	
	private static String[] StateInfos=new String[]{"扫描到","中转主机传输成功","中转主机传输失败","目标传输成功","目标传输失败"};
	

	
	public static void setStateInfos(String[] stateInfos)
	{
		StateInfos=stateInfos;
	}
	
	public static String[] getStateInfos()
	{
		return StateInfos;
	}
	
	/**
	 * 根据State判断出StateInfo
	 * @param state
	 * @return
	 */
	public static String getStateInfoByState(Integer state)
	{
		//String[] StateInfos=new String[]{"扫描到","文件无毒","有病毒","无敏感词","有敏感词",""};
		if(state!=null)
		{
			if(state>=5)
			{
				return "错误";
			}
			return StateInfos[state];
			
		}
		
		return "错误";
	}
	
	
	/**
	 * 传输真正完成的状态
	 * @return
	 */
	public static int theSucessState(boolean isTransit)
	{
		if(isTransit) //有中转传输
		{
			return 3;
		}else{
			
			return 1;
		}
	}
	
	/**
	 * 传进来的state是否是传送成功标志，中转主机成功和目标成功都返回true
	 * @param state
	 * @return
	 */
	public static boolean isSucessState(int state)
	{
		if(state==1||state==3)
		{
			return true;
		}
		
		return false;
	}
		
	
}
