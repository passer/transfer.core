
package com.filetransfer.eis;
/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-8-5 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class HostInfo {

	private String ip;
	private int port;
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	
	
}
