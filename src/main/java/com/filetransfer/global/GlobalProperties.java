
package com.filetransfer.global;

/**
 * 功能说明: 全局变量 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-22 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class GlobalProperties {

	/**
	 * 线程池线程数
	 */
	public static int THREAD_POOL_NUM=4; 
	/**
	 * 错误重传次数
	 */
	public static int FAIL_RESEND_COUNT=3;
	
	/**
	 * 传输超时时间，单位 毫秒
	 */
	public static Integer SO_TIMEOUT=3*60*60*1000; 
	/**
	 * 是否把system.out.println()的内容也打印到日志文件
	 */
	public static boolean IS_Print_SYSTEMOUT_LOG=false; 
	/**
	 * 一次批量传输最大传送文件数量，当要传的文件数量大于这个值，后面的文件会改在下次批量传输中传送
	 */
	public static int ONE_TRANSFER_MAX_SEND_COUNT=10000;
	
	
	/**
	 *  默認消息接收端口
	 */
	public static Integer DEFAULT_RECEIVE_MSG_PORT=211;
	/**
	 * 默认缓存名
	 */
	public static String DEFAULT_FILE_CACHE_NAME="defaultFileCache";
	
	/**
	 * 一次批量传输记录文本路径
	 */
	public static String LAST_TRANSGER_PATH="" ; 

	//public static String VIRUS_SACN_TXT_LOG=GlobalProperties.class.getClassLoader().getResource("").getPath()+File.separatorChar+"virus_log";
	
	static{
		
	/*	Properties sysInfo=PropertiesUtil.readPropertiesFile(SYSINFO_PROPERTIES_PATH);
		
		THREAD_POOL_NUM=PropertiesUtil.getPropertyIntegerValue(sysInfo.getProperty("THREAD_POOL_NUM"),THREAD_POOL_NUM);   
		SO_TIMEOUT=PropertiesUtil.getPropertyIntegerValue(sysInfo.getProperty("SO_TIMEOUT"),SO_TIMEOUT);*/
		//LAST_TRANSGER_PATH=GlobalProperties.class.getClassLoader().getResource("").getPath()+File.separatorChar+"lastTransfe.txt";
		
	
	}
	
}
