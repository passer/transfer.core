
package com.filetransfer.global;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 功能说明:  线程池工厂<br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-21 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ThreadPoolFactory {

	//private static ThreadPoolFactory threadPoolFactory=new ThreadPoolFactory();
	
	
	private static Integer threadNum=GlobalProperties.THREAD_POOL_NUM;
	
	private static Integer receiveMessageThreadNum=1;
	
/*	private void init()
	{
		
		executorService=Executors.newFixedThreadPool(threadNum); 
	
		
	}*/
	
	private static class ExecutorServiceClass
	{
		
		public static final ExecutorService executorService=Executors.newFixedThreadPool(threadNum);
		
		public static final ExecutorService executorServiceScan=Executors.newFixedThreadPool(2);  //专门扫描的线程
		
		public static final ExecutorService executorServiceReceiveMessage=Executors.newFixedThreadPool(receiveMessageThreadNum);  //专门扫描的线程
		
	}

	public static ExecutorService getExecutorService() {
		
		/*if(executorService==null)
		{
			synchronized (threadNum) {
				if(executorService==null)
				{
					executorService=Executors.newFixedThreadPool(threadNum); 
				}
					
			}
			
		}*/
		
		return ExecutorServiceClass.executorService;
	}


	public static ExecutorService getExecutorServiceScan() {
		
		return ExecutorServiceClass.executorServiceScan;
	}
	
	public static ExecutorService getExecutorServiceReceiveMessage() {
		
		return ExecutorServiceClass.executorServiceReceiveMessage;
	}
	
}
