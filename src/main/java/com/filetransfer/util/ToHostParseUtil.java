
package com.filetransfer.util;

import com.filetransfer.eis.HostInfo;



/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-8-5 <br>
 * 相关文档:  <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ToHostParseUtil {

	public static final String MSG_PORT_REG="msgport[";
	public static final String MSG_IP_REG="msgip[";
	public static final String THROUGH_REG="through[";
	public static final String TARGET_REG="target[";
	
	/**
	 * 是否是中转传输 ，针对A服务器
	 * @param toHostParseInfo
	 * @return
	 */
	public static boolean isTransitTransfer(String toHostParseInfo)
	{
		if(toHostParseInfo.indexOf(MSG_PORT_REG)>-1)
		{
			return true;
		}
		
		return false;
	}
	
	/**
	 * A服务器消息接收端口
	 * @param toHostParseInfo
	 * @return
	 */
	public static int aReceiveMsgPort(String toHostParseInfo)
	{
		int msgIndex=toHostParseInfo.indexOf(MSG_PORT_REG);
		if(msgIndex>-1)
		{
			String port=toHostParseInfo.substring(msgIndex+MSG_PORT_REG.length(), toHostParseInfo.indexOf("]",msgIndex));
			
			return Integer.parseInt(port);
		}
		
		return 222;
	}
	
	public static HostInfo getReceiveMsgHostInfo(String toHostParseInfo)
	{
		int msgportIndex=toHostParseInfo.indexOf(MSG_PORT_REG);
		int msgipIndex=toHostParseInfo.indexOf(MSG_IP_REG);
		if(msgportIndex>-1)
		{
			String port=toHostParseInfo.substring(msgportIndex+MSG_PORT_REG.length(), toHostParseInfo.indexOf("]",msgportIndex));
			
			HostInfo hostInfo=new HostInfo();	
			hostInfo.setPort(Integer.parseInt(port));
			if(msgipIndex>-1)
			{
				String ip=toHostParseInfo.substring(msgipIndex+MSG_IP_REG.length(), toHostParseInfo.indexOf("]",msgipIndex));
				hostInfo.setIp(ip);
			}
		
			
			return hostInfo;
		}
		
		return null;
	}
	
	/**
	 * 目标传送的host信息
	 * @param toHostParseInfo
	 * @return
	 */
	public static HostInfo getTargetHostInfo(String toHostParseInfo)
	{
		int targetIndex=toHostParseInfo.indexOf(TARGET_REG);
		if(targetIndex>-1)
		{
			String hostStr=toHostParseInfo.substring(targetIndex+TARGET_REG.length(), toHostParseInfo.indexOf("]",targetIndex));
			String[] hostArr=hostStr.split(":");
			HostInfo hostInfo=new HostInfo();
			hostInfo.setIp(hostArr[0]);
			hostInfo.setPort(Integer.parseInt(hostArr[1]));
			return hostInfo;
			
		}
		
		return null;
	}
	/**
	 * 中转传送的host信息
	 * @param toHostParseInfo
	 * @return
	 */
	public static HostInfo getThroughHostInfo(String toHostParseInfo)
	{
		int throughIndex=toHostParseInfo.indexOf(THROUGH_REG);
		if(throughIndex>-1)
		{
			String hostStr=toHostParseInfo.substring(throughIndex+THROUGH_REG.length(), toHostParseInfo.indexOf("]",throughIndex));
			String[] hostArr=hostStr.split(":");
			HostInfo hostInfo=new HostInfo();
			hostInfo.setIp(hostArr[0]);
			hostInfo.setPort(Integer.parseInt(hostArr[1]));
			return hostInfo;
			
		}
		
		return null;
	}

	public static String parseTargetHostString(HostInfo targetInfo)
	{
		
		if(targetInfo!=null)
		{
			String parseStr=TARGET_REG+targetInfo.getIp()+":"+targetInfo.getPort()+"]";
			return parseStr;
		}
		
		return "";
	}
	
	
	
	public static void main(String[] args) {
		String info="through[localhost:112] target[localhost:112] msgport[223]";
		System.out.println(aReceiveMsgPort(info));
		
	}
	
}
