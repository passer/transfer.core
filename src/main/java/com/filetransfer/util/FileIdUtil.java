
package com.filetransfer.util;
/**
 * 功能说明:  生成文件id<br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-8-4 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileIdUtil {
	
	/**
	 * 输出的固定长度
	 */
	private static final int NLen=11;
	
	/**
	 * 填充字符
	 */
	private static final char defaultChar='a';
	
	/**
	 * 根据文件路径和文件大小生成文件id
	 * @param filePath
	 * @param fileLength
	 * @return 文件id
	 */
	public static String createFileId(String filePath,String fileLength,Long lastModified)
	{
		String filePathNum=createNLengthString(filePath.hashCode()+"");
		String fileLengthNum=createNLengthString(fileLength);
		long lastModifiedNum=lastModified%1000000;
		
		return filePathNum+fileLengthNum+lastModifiedNum;
	}
	
	
	/**
	 * 生成固定长度的字符串，不足的用默认字符填充
	 * @param numStr
	 * @return
	 */
	private static String createNLengthString(String numStr)
	{
		int len=NLen;
		
		if(numStr.length()>NLen)
		{
			len=numStr.length();
			
		}
		
		char[] newNumArr=new char[len];
		for(int i=0;i<newNumArr.length;i++)
		{
			newNumArr[i]=defaultChar;
			
		}
		
		char[] numArr=numStr.toCharArray();
		
		for(int i=0;i<numArr.length;i++)
		{
			newNumArr[i]= numArr[i];
			
		}
		
		return String.valueOf(newNumArr);
	}
	
	public static void main(String[] args) {
		
		
		//String st=createNLengthString("23334444");
		//
		long l=System.currentTimeMillis();
		String st=createFileId("E:\\upload\\Ol.vmdk", "383264",l);
		System.out.println(st);
	}

}
