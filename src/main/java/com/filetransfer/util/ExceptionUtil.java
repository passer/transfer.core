
package com.filetransfer.util;
/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ExceptionUtil {

	/**
	 * 抛出异常
	 * @param message
	 * @param cause
	 */
	public static void throwException(String message, Throwable cause)
	{
		
		throw new RuntimeException(message,cause);
		
	}
	
	
	/**
	 * 抛出异常
	 * @param message
	 */
	public static void throwException(String message)
	{
		
		throw new RuntimeException(message);
		
	}
	
	
}
