
package com.filetransfer.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-22 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class PropertiesUtil {

	
	  /**
	   * //读取资源文件
	   * @param propertieFilename  资源名
	   * @param propertieKey
	   * @return
	   */
	    public static String readPropertiesFile(String propertieFilename,String propertieKey)  
	    {  
	        Properties properties = new Properties();  
	        InputStream inputStream=null;
	        try  
	        {  
	        	//inputStream = new FileInputStream(propertieFilename);  
	        	inputStream =FileUtil.class.getResourceAsStream(propertieFilename);
	            properties.load(inputStream);
	        }  
	        catch (IOException e)  
	        {  
	            e.printStackTrace();  
	        }finally{
	        	if(inputStream!=null)
	        	{
	        		  try {
	  					inputStream.close();
	  				} catch (IOException e) {
	  					e.printStackTrace();
	  				} 
	        	}//关闭流  
	        	
	        }
	        String value = properties.getProperty(propertieKey);  
	        
	        return value;
	    } 
	
    /**
	   * //读取资源文件
	   * @param propertieFilename  资源名
	   * @param propertieKey
	   * @return
	   */
	    public static Properties readPropertiesFile(String propertieFilename)  
	    {  
	        Properties properties = new Properties();  
	        InputStream inputStream=null;
	        try  
	        {  
	        	inputStream = new FileInputStream(propertieFilename);  
	        	//inputStream =FileUtil.class.getResourceAsStream(propertieFilename);
	            properties.load(inputStream);
	        }  
	        catch (IOException e)  
	        {  
	            e.printStackTrace();  
	        }finally{
	        	if(inputStream!=null)
	        	{
	        		  try {
	  					inputStream.close();
	  				} catch (IOException e) {
	  					e.printStackTrace();
	  				} 
	        	}//关闭流  
	        	
	        }
	   
	        
	        return properties;
	    } 
	    
	    /**
	     * 获得配置的值，如果对应没有设值，则为默认值
	     * @param propertValue
	     * @param defaultValue
	     * @return
	     */
	    public static String getPropertyStringValue(String propertValue,String defaultValue)
	    {
	    	if(propertValue==null)
	    	{
	    		return defaultValue;
	    	}
	    	
	    	return propertValue;
	    }
	
	    /**
	     * 获得配置的int值，如果对应没有设值，则为默认值
	     * @param propertValue
	     * @param defaultValue
	     * @return
	     */
	    public static Integer getPropertyIntegerValue(String propertValue,Integer defaultValue)
	    {
	    	if(propertValue==null)
	    	{
	    		return defaultValue;
	    	}
	    	
	    	return Integer.parseInt(propertValue);
	    }
	    
	    /**
	     * 获得配置的boolean值，如果对应没有设值，则为默认值
	     * @param propertValue
	     * @param defaultValue
	     * @return
	     */
	    public static boolean getPropertyBooleanValue(String propertValue,boolean defaultValue)
	    {
	    	if(propertValue==null)
	    	{
	    		return defaultValue;
	    	}
	    	
	    	return Boolean.parseBoolean(propertValue);
	    }
	    
}
