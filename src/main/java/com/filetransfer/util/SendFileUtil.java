
package com.filetransfer.util;
/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-31 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class SendFileUtil {

	/**
	 * 获得文件发送文件夹的相对路径
	 * @param sendFilePath
	 * @param absolutePath
	 * @return
	 */
	public static String getPathFileName(String sendFilePath,String absolutePath) {
		String fileName = null;
		if (absolutePath != null) {
			// GlobalProperties.SEND_FILE_PATH
			fileName = absolutePath.substring(sendFilePath.length());
		}

		return fileName;
	}
	
	
	/**
	 * 获得文件发送文件夹的相对路径
	 * @param sendFilePath
	 * @param absolutePath
	 * @return
	 */
	public static String getDefaultFileName(String absolutePath) {
		String fileName = null;
		if (absolutePath != null) {
			// GlobalProperties.SEND_FILE_PATH
			fileName = absolutePath.substring(absolutePath.indexOf(":")+1);
		}

		return fileName;
	}
	
	public static void main(String[] args) {
		
		System.out.println(getDefaultFileName("D:\\BACKTRACK3_VMWare\\dffd"));
	}
	
	
}
