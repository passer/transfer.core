
package com.filetransfer.util;
/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-10-10 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class AuthorityUtil {

	/**
	 *  计算加密后的字符串
	 * @param key 密钥。
	 * @param id  文件id
	 * @param time  时间
	 * @return
	 */
	public static String getAuthorityCode(String key,String id,long time)
	{
		
		if(StringUtil.isAllNotEmpty(key,id,time))
		{
			String sign=id+time+key;
			Encrypt abc=new Encrypt();
			 abc.setKey(key);    //调用set函数设置密钥。
			 abc.setEncString(sign);//将要加密的明文传送给Encrypt.java进行加密计算。
			 String Mi=abc.getStrMi();  //调用get函数获取加密后密文。
			return Mi;
		}else{
			
			LogUtil.err(" AuthorityUtil key,id,time中不能为null");
		}
		
		return null;
	}
	
}
