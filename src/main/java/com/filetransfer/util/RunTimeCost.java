
package com.filetransfer.util;
/**
 * 功能说明: 统计执行时间 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-8-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class RunTimeCost {

	private long startTime;
	
	
	public RunTimeCost()
	{
		startTime();
	}
	
	/**
	 * 计时开始
	 */
	public void startTime()
	{
		startTime=System.currentTimeMillis();
	}
	
	/**
	 * 运行到此处花费的时间
	 * @return
	 */
	public long toNowCostTime()
	{
		long endTime=System.currentTimeMillis();
		
		long costTime=endTime-startTime;
		return costTime;
	}
	
}
