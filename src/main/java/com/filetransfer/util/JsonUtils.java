package com.filetransfer.util;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;



public class JsonUtils {



	    public static String toJson(Object obj) {

	        return JSON.toJSONString(obj);
	    }
	    /**
	     *
	     * @param obj
	     * @param features
	     * @return
	     */
	    public static String toJson(Object obj, SerializerFeature... features) {
	        return JSON.toJSONString(obj, features);
	    }

	    /**
	     * 
	     * @param json
	     * @param clazz
	     * @return
	     */
	    public static <T> T fromJson(String json, Class<T> clazz) {

	        return JSON.parseObject(json, clazz);
	    }

	 
}
