
package com.filetransfer.util;

import java.io.File;

import com.filetransfer.transport.command.FileInfoMessage;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-23 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ReceiveFileUtil {

	/**
	 * 确定文件是否完整接收成功
	 * @param receiveMessage
	 * @param localReceiveFile
	 * @param isMd5Check  是否有md5校验
	 * @return
	 */
	public static boolean checkIsReceiveSucess(FileInfoMessage receiveMessage,File localReceiveFile,boolean isMd5Check)
	{
		if(receiveMessage==null)
		{
			LogUtil.err("接收 FileInfoMessage为空");
			
			return false;
		}
		
		if(isMd5Check)
		{
			String nowFileMd5=MD5Util.getMD5(localReceiveFile);
			if(nowFileMd5.equals(receiveMessage.getMd5()))  //md5相等
			{
				
				return true;
			}else{
				LogUtil.echo(receiveMessage.getAbsolutePath()+" 接收后md5不相同 ,接收文件MD5："+receiveMessage.getMd5()+" 当前文件MD5"+nowFileMd5);
							
			}
			
			
		}else{
			
			long localFileSize=localReceiveFile.length();
			
			if(localFileSize==receiveMessage.getFileSize())
			{
				return true;
			}else{
				
				LogUtil.echo(receiveMessage.getAbsolutePath()+" 接收后文件大小不相同,本应该大小："+receiveMessage.getFileSize()+" 当前文件大小 "+localFileSize);
				
			}
			
		}
		
		return false; 
		
	}
	
	
}
