
package com.filetransfer.util;
/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-17 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class StringUtil {

	/**
	 * 判断是否为空
	 * @param value
	 * @return
	 */
	public static boolean isNotEmpty(Object value)
	{
		
		if(value!=null)
		{
			if(value instanceof String )
			{
				if(!"".equals(value))
				{
					
					return true;
				}
				
			}else{
				return true;
			}
			
			
		}
		
		
		
		return false;
	}
	
	/**
	 * 是否全是不为空
	 * @param params
	 * @return
	 */
	public static boolean isAllNotEmpty(Object ...params)
	{
		boolean flag=true;
		for(int i=0;i<params.length;i++)
		{
			if(!isNotEmpty(params[i]))
			{
				flag=false;
				break;
			}
		}
		
		
		return flag;
	}
	
}
