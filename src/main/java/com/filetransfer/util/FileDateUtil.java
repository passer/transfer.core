package com.filetransfer.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileDateUtil {

	/**
	 * date转字符串
	 * @param date
	 * @param format
	 * @return
	 */
	public static String date2Str(Date date, String format) {
		  if (null == date) {
		   return null;
		  }
		  SimpleDateFormat sdf = new SimpleDateFormat(format);
		  return sdf.format(date);
	}
	
	/**
	 * date转字符串，默认格式yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @return
	 */
	public static String date2StrDefaultTime(Date date)
	{
		String format="yyyy-MM-dd HH:mm:ss";	
		return date2Str(date,format);
	}
	
	/**
	 * date转字符串，默认格式yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public static String date2StrDefaultDate(Date date)
	{
		String format="yyyy-MM-dd";	
		return date2Str(date,format);
	}
	
	/**
	 * 字符串转日期   格式 yyyy-MM-dd hh:mm:ss
	 * @param dateStr
	 * @return
	 */
	public static Date str2date(String dateStr)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		try {
			Date date=sdf.parse(dateStr);
			
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
		return null;
	}
	
}
