
package com.filetransfer.util;

import java.io.File;
import java.sql.Timestamp;

import com.filetransfer.eis.FileInfo;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileInfoUtil {

	
	/**
	 * 填补fileInfo信息，如果以下属性不存在，会根据FilePath来计算，
	 * @param fileInfo  FilePath不能为空
	 */
	public static void putFileInfoPro(FileInfo fileInfo,boolean isMd5Check)
	{
		if(fileInfo==null||fileInfo.getFilePath()==null)
		{
			throw new RuntimeException("fileInfo不能为空或fileInfo.getFilePath()不能为空");
		}
		File file=new File(fileInfo.getFilePath());
		if(fileInfo.getFileSize()==null)
		{
	    	fileInfo.setFileSize(file.length()+"");
		}
		if(fileInfo.getFileName()==null)
		{
			fileInfo.setFileName(file.getName());
		}	
		if(fileInfo.getLastModified()==null)
		{
			fileInfo.setLastModified(file.lastModified());
		}
		if(fileInfo.getId()==null)
		{
			
			fileInfo.setId(FileIdUtil.createFileId(fileInfo.getFilePath(), fileInfo.getFileSize(),fileInfo.getLastModified())); 
		}
		if(fileInfo.getCreateDttm()==null)
		{
			Timestamp createDttm = new Timestamp(System.currentTimeMillis()); 
	    	fileInfo.setCreateDttm(createDttm);
		}
		
		if(isMd5Check&&fileInfo.getMd5()==null)
		{
				String md5=MD5Util.getMD5(file);
				fileInfo.setMd5(md5);
		}
		
	}
	
}
