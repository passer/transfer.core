
package com.filetransfer.cache;

import java.io.File;

import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class CacheUtil {

	public static void changeDataLastModified(net.sf.ehcache.CacheManager cacheManager,String cacheName)
	{
		
		File index = cacheManager.getDiskStorePathManager().getFile(cacheName, ".index");
        File data  = cacheManager.getDiskStorePathManager().getFile(cacheName, ".data");
        if(index!=null&&data!=null)
        {
        	if(index.lastModified()!=data.lastModified())
        	{
        		 data.setLastModified( index.lastModified());
        	}
        	 
        }
		
	}
	
	
	public static Cache createEhCacheToManage(net.sf.ehcache.CacheManager cacheManager,String cacheName)
	{
		
		changeDataLastModified(cacheManager,cacheName);
		CacheConfiguration configuration=new CacheConfiguration(cacheName, CacheConfig.cacheMaxElements)  
	    .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LFU)  
	    .overflowToDisk(true)  
	    .maxElementsInMemory(1)
	    .eternal(true)  
	  /* .timeToLiveSeconds(2260)  
	    .timeToIdleSeconds(2230) */ 
	    .diskPersistent(true) ;
	    //.diskExpiryThreadIntervalSeconds(0);
		Cache cache=new Cache(configuration);	
		cacheManager.addCache(cache);
		
		return cache;
	}
	
}
