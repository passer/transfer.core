
package com.filetransfer.cache;

import java.io.Serializable;
import com.filetransfer.util.LogUtil;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-26 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class CacheManager {
    
    private static CacheProvider provider;
 
    static {
        initCacheProvider("com.filetransfer.cache.EhCacheProvider");
    }
     
    private static void initCacheProvider(String prv_name){
        try{
            CacheManager.provider = (CacheProvider)Class.forName(prv_name).newInstance();
            CacheManager.provider.start();
            LogUtil.echo("Using CacheProvider : " + provider.getClass().getName());
        }catch(Exception e){
            //LogUtil.err("Unabled to initialize cache provider:" + prv_name + ", using ehcache default.", e);
            CacheManager.provider = new EhCacheProvider();
        }
    }
 
    private final static Cache _GetCache(String cache_name, boolean autoCreate) {
        if(provider == null){
            provider = new EhCacheProvider();
        }
        return provider.buildCache(cache_name, autoCreate);
    }
 
    /**
     * 获取缓存中的数据
     * @param name
     * @param key
     * @return
     */
    public final static Object get(String name, Serializable key){
        //System.out.println("GET1 => " + name+":"+key);
        if(name!=null && key != null)
            return _GetCache(name, true).get(key);
        return null;
    }
     
    /**
     * 获取缓存中的数据
     * @param <T>
     * @param resultClass
     * @param name
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
    public final static <T> T get(Class<T> resultClass, String name, Serializable key){
        //System.out.println("GET2 => " + name+":"+key);
        if(name!=null && key != null)
            return (T)_GetCache(name, true).get(key);
        return null;
    }
     
    /**
     * 写入缓存
     * @param name
     * @param key
     * @param value
     */
    public final static void set(String name, Serializable key, Serializable value){
        //System.out.println("SET => " + name+":"+key+"="+value);
        if(name!=null && key != null && value!=null)
            _GetCache(name, true).put(key, value);      
    }
     
    /**
     * 清除缓冲中的某个数据
     * @param name
     * @param key
     */
    public final static void evict(String name, Serializable key){
        if(name!=null && key != null)
            _GetCache(name, true).remove(key);      
    }
    
    public final static void flush(String name){
        if(name!=null)
        {
        	 _GetCache(name, true).flush();  
        	 ((EhCacheProvider)provider).get_CacheManager().remove(name);
        	 ((EhCacheProvider)provider).getManager().removeCache(name);
        }
               
    }
 
    /**
     * 清除缓冲中的某个数据
     * @param name
     * @param key
     */
    public final static void justEvict(String name, Serializable key){
        if(name!=null && key != null){
            Cache cache = _GetCache(name, false);
            if(cache != null)
                cache.remove(key);
        }
    }
    
    
    public static void stop()
    {
    	provider.stop();
    }
 
    public static void main(String[] args) {
		
    	String cacheName="testCache";
    	 String key="key";
    	 String value="oschina2222";
    	 //set(cacheName, key, value);
    	 String gValue = (String)get(cacheName, key);//A
    	 if(gValue!=null){
    	 System.out.println("获取缓存成功："+gValue);
    	 }else{
    	 System.out.println("获取缓存为空，设置缓存");
    	 set(cacheName, key, value);
    	 System.out.println("设置缓存之后获取缓存："+(String)get(cacheName, key));
    	 } 
    	 
  
    	
	}
    
    
}
