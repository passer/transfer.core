
package com.filetransfer.cache;

import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.DiskStoreConfiguration;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-28 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class CacheConfig {

	public static String memoryStoreEvictionPolicy="LFU";
	
	/**
	 * 缓存路径名
	 */
	public static String diskStore="esbcache";
	
	public static int cacheMaxElements=1000; 
	
	
	public static Configuration getConfiguration()
	{
		
		Configuration config=new Configuration();
		
		//如果不使用ehcache.xml配置文件，那么必须用代码配置一个defaultCacheConfiguration
		CacheConfiguration defaultCacheConfiguration=new CacheConfiguration();
		//defaultCacheConfiguration.setMaxEntriesLocalHeap(0);
		defaultCacheConfiguration.setEternal(true);
		/*defaultCacheConfiguration.setTimeToIdleSeconds(30);
		defaultCacheConfiguration.setTimeToLiveSeconds(30);*/
		defaultCacheConfiguration.setMemoryStoreEvictionPolicy(memoryStoreEvictionPolicy);
		//defaultCacheConfiguration.setMaxElementsInMemory(1);
		config.addDefaultCache(defaultCacheConfiguration);//设置默认cache
		
		DiskStoreConfiguration diskStoreConfigurationParameter=new DiskStoreConfiguration();
		diskStoreConfigurationParameter.setPath(diskStore);
		config.diskStore(diskStoreConfigurationParameter);

		
		return config;
	}
	
	

	
	
}
