
package com.filetransfer.cache;


import java.io.File;
import java.io.Serializable;
import com.filetransfer.util.FileIdUtil;
import com.filetransfer.util.LogUtil;
import com.filetransfer.util.StringUtil;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-26 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileCacheFactory {


	/**
	 * 把数据放入缓存
	 * @param cacheName 缓存名
	 * @param key
	 * @param value
	 */
	public static synchronized void putValue(String cacheName,Serializable key, Serializable value)
	{
		com.filetransfer.cache.CacheManager.set(cacheName, key, value);
		
	}
	/**
	 * 从缓存取出数据
	 * @param cacheName 缓存名
	 * @param key
	 * @return
	 */
	public static Object getValue(String cacheName,Serializable key)
	{
		return com.filetransfer.cache.CacheManager.get(cacheName, key);
	}
	/**
	 * 从缓存删除数据
	 * @param cacheName 缓存名
	 * @param key
	 */
	public static void remove(String cacheName,Serializable key)
	{
		com.filetransfer.cache.CacheManager.evict(cacheName, key);
	}
	
	
	/**
	 * 从缓存中获取文件状态
	 * @param fileInfoId
	 * @return 最近修改时间
	 */
	public static Integer getFileInfoIdById(String cacheName,String fileInfoId)
	{
		if(fileInfoId==null)
		{
			return null;
		}
		Object ob=getValue(cacheName,fileInfoId);
		if(ob!=null){
			//SysLogger.getLogger().debug(101, new String[]{"文件信息","fileInfoId="+fileInfoId});
			//LogUtil.echo("fileInfoId"+fileInfoId+"获得缓存"+ob);
			return (Integer)ob;			
		}
		return null;
		
	}
	
	/**
	 * 从缓存中获取文件修改时间
	 * @param file
	 * @return
	 */
	public static Integer getFileInfoIdByFile(String cacheName,File file)
	{
		if(file==null)
		{
			return null;
		}
		String fileInfoId=FileIdUtil.createFileId(file.getAbsolutePath(),
				file.length()+"",file.lastModified());
		return getFileInfoIdById(cacheName,fileInfoId);
		
	}
	
	
	public static void removeFileInfo(String cacheName,String fileInfoId)
	{
		if(getValue( cacheName,fileInfoId)!=null){
			remove(cacheName,fileInfoId);
		}
		
	}
	
	/**
	 * 向缓存添加文件id
	 * @param fileInfoId
	 * @param lastModified
	 */
	public static void addFileInfoId(String cacheName,String fileInfoId,Integer fileState)
	{
	
		if(StringUtil.isAllNotEmpty(fileInfoId,fileState))
		{
			putValue(cacheName,fileInfoId, fileState);
		}else{
			
			LogUtil.err("缓存添加fileInfoId,fileState 不能为空");
		}
		
	}
		
	
	public static void main(String[] args) {
		int maxElements=10000;
		
		
		CacheConfiguration configuration=new CacheConfiguration("testCache", maxElements)  
	    .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LFU)  
	    .overflowToDisk(true)  
	    .eternal(true)  
	   .timeToLiveSeconds(2260)  
	    .timeToIdleSeconds(2230)  
	    .diskPersistent(true) ;
	    //.diskExpiryThreadIntervalSeconds(0);
		
		
	
	
		
		//net.sf.ehcache.CacheManager cacheManager=CacheManager.create( CacheManager.class.getResource("/ehcache.xml"));
		
		String cacheName="testCache";
		
      
		CacheConfig cacheConfig=new CacheConfig();
		
		
		net.sf.ehcache.CacheManager cacheManager=CacheManager.create(cacheConfig.getConfiguration());
		
		
		/*File index = new File( diskStoreConfigurationParameter.getPath(), cacheName + ".index" );
        File data  = new File( diskStoreConfigurationParameter.getPath(), cacheName+ ".data"  );
        if(index!=null&&data!=null)
        {
        	if(index.lastModified()!=data.lastModified())
        	{
        		 data.setLastModified( index.lastModified());
        	}
        	 
        }*/
		
		
	//	Cache testCache=new Cache(configuration);
		
		//cacheManager.addCache("testCache");
		
	//	cacheManager.addCache(testCache);
		Cache testCache=CacheUtil.createEhCacheToManage(cacheManager, cacheName);
	
		
		//Cache testCache=cacheManager.getCache(cacheName);
		
		for(int i=0;i<200;i++)
		{
			
			testCache.put(new Element("key"+i, "value"+i)); 
			
			System.out.println("put key"+i);
			
			
			
			
		}
		testCache.flush();
	
/*	testCache.put(new Element("key1", "value1")); 
	//testCache.flush();
		testCache.put(new Element("key2", "value2"));
		testCache.flush();*/
		
	//testCache.flush();
		for(int i=0;i<200;i++)
	{
		System.out.println(testCache.get("key"+i).getObjectValue());
		
	}
	System.out.println(testCache.get("key2").getObjectValue());
	testCache.flush();
//	System.out.println(testCache.get("key1").getObjectValue());
	//System.out.println(testCache.get("key1").getObjectValue());
	//testCache.flush();
		
	
	//cacheManager.shutdown();
	
		
	}
	
}
