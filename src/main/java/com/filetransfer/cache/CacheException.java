
package com.filetransfer.cache;
/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-26 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class CacheException extends RuntimeException {
	 
    public CacheException(String s) {
        super(s);
    }
 
    public CacheException(String s, Throwable e) {
        super(s, e);
    }
 
    public CacheException(Throwable e) {
        super(e);
    }
     
}
