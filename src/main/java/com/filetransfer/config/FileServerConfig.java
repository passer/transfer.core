
package com.filetransfer.config;

import com.filetransfer.global.GlobalProperties;
import com.filetransfer.server.fileserver.FileReceiveCallBack;

/**
 * 功能说明: 文件接收配置类 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileServerConfig {

	/**
	 * 接收到文件保存的文件夹
	 */
	private String receiveFileSavePath;
	/**
	 * 接收文件绑定的端口
	 */
	private Integer receiveFilePort;
	/**
	 * 是作为中转服务器还是作为实际接收服务器
	 */
	private boolean isTransitServer;
	
	/**
	 * 超时时间
	 */
	private int soTimeout=GlobalProperties.SO_TIMEOUT;
	
	/**
	 * 线程池大小
	 */
	private int poolSize=4;
	
	/**
	 * 是否进行安全权限验证，为true会开启权限验证，其内部为增加一个验证拦截器
	 */
	private boolean isAuthorityCheck=false;
	/**
	 * 权限验证密锁，必须isAuthorityCheck为true时有效。
	 */
	private String authorityKey="";
	
	
	
	
	/**
	 * 构造函数，都不能为空
	 * @param receiveFileSavePath
	 * @param receiveFilePort
	 * @param isTransitServer
	 */
	public FileServerConfig(String receiveFileSavePath,
			Integer receiveFilePort, boolean isTransitServer) {
		super();
		this.receiveFileSavePath = receiveFileSavePath;
		this.receiveFilePort = receiveFilePort;
		this.isTransitServer = isTransitServer;
	}

	




	public String getReceiveFileSavePath() {
		return receiveFileSavePath;
	}

	public void setReceiveFileSavePath(String receiveFileSavePath) {
		this.receiveFileSavePath = receiveFileSavePath;
	}

	public Integer getReceiveFilePort() {
		return receiveFilePort;
	}

	public void setReceiveFilePort(Integer receiveFilePort) {
		this.receiveFilePort = receiveFilePort;
	}

	public boolean isTransitServer() {
		return isTransitServer;
	}

	public void setTransitServer(boolean isTransitServer) {
		this.isTransitServer = isTransitServer;
	}

	public int getSoTimeout() {
		return soTimeout;
	}

	public void setSoTimeout(int soTimeout) {
		this.soTimeout = soTimeout;
	}

	public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}






	public boolean isAuthorityCheck() {
		return isAuthorityCheck;
	}






	public void setAuthorityCheck(boolean isAuthorityCheck) {
		this.isAuthorityCheck = isAuthorityCheck;
	}






	public String getAuthorityKey() {
		return authorityKey;
	}






	public void setAuthorityKey(String authorityKey) {
		this.authorityKey = authorityKey;
	}


	
	
	
}
