
package com.filetransfer.config;

import com.filetransfer.server.message.ReceiveFileMessageCallBack;

/**
 * 功能说明:  <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ReceiveFileMessageConfig {

	/**
	 * 消息接收端口
	 */
	private Integer bindPort; 
	
	/**
	 * 回调
	 */
	private ReceiveFileMessageCallBack receiveFileMessageCallBack;
	
	
	

	public ReceiveFileMessageConfig(Integer bindPort,
			ReceiveFileMessageCallBack receiveFileMessageCallBack) {
		super();
		this.bindPort = bindPort;
		this.receiveFileMessageCallBack = receiveFileMessageCallBack;
	}

	public Integer getBindPort() {
		return bindPort;
	}

	public void setBindPort(Integer bindPort) {
		this.bindPort = bindPort;
	}

	public ReceiveFileMessageCallBack getReceiveFileMessageCallBack() {
		return receiveFileMessageCallBack;
	}

	public void setReceiveFileMessageCallBack(ReceiveFileMessageCallBack receiveFileMessageCallBack) {
		this.receiveFileMessageCallBack = receiveFileMessageCallBack;
	}
	
}
