
package com.filetransfer.config;

import com.filetransfer.global.GlobalProperties;

/**
 * 功能说明: 文件传输客户端配置 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-9-24 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileClientConfig {

	/**
	 * 要监控的文件夹，当isMonitor=true有效
	 */
	private String MonitorPath; 
	/**
	 * 是否监控文件夹，如果true则是监控文件夹，批量传输；false则发送单文件
	 */
	private boolean isMonitor;
	/**
	 * 缓存名，一个FileClientConfig一定要不同的缓存名，不然监控时对比会发生错误
	 */
	private String cacheName;
	
	/**
	 * 是否用md5校验
	 */
	private boolean IsMd5Check=true; 
	/**
	 * 是否进行安全权限验证，为true会开启权限验证，其内部为增加一个验证拦截器
	 */
	private boolean isAuthorityCheck=false;
	/**
	 * 权限验证密锁，必须isAuthorityCheck为true时有效。
	 */
	private String authorityKey="";
	
	/**
	 * 是否中转传输
	 */
	private boolean isTransit=false;  
	/**
	 * 文件要发送给的主机ip，如果isTransit为true是中转服务器ip，false则为目标ip
	 */
	private String sendToIp; 
	/**
	 * 文件要发送给的主机端口，如果isTransit为true是中转服务器端口，false则为目标端口
	 */
	private Integer sendToPort;
	/**
	 * 目标主机的ip，如果isTransit为true则无效
	 */
	private String targetIp; 
	/**
	 * 目标主机的端口，如果isTransit为true则无效
	 */
	private Integer targetPort; 
	/**
	 *  接收消息端口
	 */
	private Integer receiveMsgPort=GlobalProperties.DEFAULT_RECEIVE_MSG_PORT;
	/**
	 * 传输到服务器的配置信息，如果有through[]和msgport[],则为中转传输。如果只是两台服务器之间传输，只要写target[]
	 * 如果该值不为空，则中转服务器里不会判断 targetIp，targetPort和receiveMsgPort
	 * 
	 */
	private String transferToHostParseInfo;
	
	
	
	
	/**
	 * 超时时间,毫秒
	 */
	private int sendTimeout=GlobalProperties.SO_TIMEOUT; 
	/**
	 * 定时任务每隔多少时间执行一次，单位毫秒
	 */
	private int timerTaskPeriod=5000; 
	
	private int failResendCount=GlobalProperties.FAIL_RESEND_COUNT;
	
	/**
	 * 是否用线程池发送，文件夹监控的情况下要false，文件夹监控本身就设了固定线程
	 */
	private boolean isThreadPoolSend=true;
	
	/**
	 * 进行批量传输时队列大小，队列多时会put会进行堵塞，
	 */
	private int blockingQueueSize=100;
	/**
	 * 进行批量传输时发送文件的线程数量
	 */
	private int sendThreadNum=4;
	
	/**
	 * 若isMonitor为false，isTransit为true，则进行中转，单文件传输
	 * @param isMonitor
	 * @param isMd5Check
	 * @param isTransit
	 * @param sendToIp 默认sendToIp，后面发送文件时 若未设置，则用此值；  若另外设置了sendToIp，则不会用此值
	 * @param sendToPort 默认sendToPort，后面发送文件时 若未设置，则用此值；  若另外设置了sendToIp，则不会用此值
	 * @param targetIp 目标主机的ip，默认targetIp
	 * @param targetPort 目标主机的port，默认 targetPort
	 * @param receiveMsgPort
	 */
	public FileClientConfig(boolean isMonitor, boolean isMd5Check,
			boolean isTransit, String sendToIp, Integer sendToPort,
			String targetIp, Integer targetPort, Integer receiveMsgPort) {
		super();
		this.isMonitor = isMonitor;
		IsMd5Check = isMd5Check;
		this.isTransit = isTransit;
		this.sendToIp = sendToIp;
		this.sendToPort = sendToPort;
		this.targetIp = targetIp;
		this.targetPort = targetPort;
		this.receiveMsgPort = receiveMsgPort;
	}

	
	/**
	 * 若isMonitor为true，isTransit为false，则不中转，监控文件夹传输
	 * @param monitorPath
	 * @param isMonitor
	 * @param isMd5Check
	 * @param isTransit
	 * @param sendToIp 默认sendToIp，后面发送文件时 若未设置，则用此值；  若另外设置了sendToIp，则不会用此值
	 * @param sendToPort 默认sendToPort，后面发送文件时 若未设置，则用此值；  若另外设置了sendToIp，则不会用此值
	 */
	public FileClientConfig(String monitorPath, boolean isMonitor,
			boolean isMd5Check, boolean isTransit, String sendToIp,
			Integer sendToPort) {
		super();
		MonitorPath = monitorPath;
		
		this.isMonitor = isMonitor;
		
		if(isMonitor)
		{
			cacheName="cache"+MonitorPath.hashCode();
		}
		
		IsMd5Check = isMd5Check;
		this.isTransit = isTransit;
		this.sendToIp = sendToIp;
		this.sendToPort = sendToPort;
	}



	/**
	 * 若isMonitor为false，isTransit为false，则不中转，单文件传输
	 * @param isMonitor
	 * @param isMd5Check
	 * @param isTransit
	 * @param sendToIp 默认sendToIp，后面发送文件时 若未设置，则用此值；  若另外设置了sendToIp，则不会用此值
	 * @param sendToPort 默认sendToPort，后面发送文件时 若未设置，则用此值；  若另外设置了sendToIp，则不会用此值
	 */
	public FileClientConfig(boolean isMonitor, boolean isMd5Check,
			boolean isTransit, String sendToIp,Integer sendToPort) {
		super();
		this.isMonitor = isMonitor;
		IsMd5Check = isMd5Check;
		this.isTransit = isTransit;
		this.sendToIp = sendToIp;
		this.sendToPort = sendToPort;
	}
	
	
	
	
	/**
	 * 用于单文件传输。发送给的ip不固定
	 * @param isMonitor
	 * @param isMd5Check
	 * @param isTransit
	 */
	public FileClientConfig(boolean isMonitor, boolean isMd5Check,
			boolean isTransit) {
		super();
		this.isMonitor = isMonitor;
		IsMd5Check = isMd5Check;
		this.isTransit = isTransit;
	}


	/**
	 * 若isMonitor为true，isTransit为true，则进行中转，监控文件夹传输
	 * @param monitorPath
	 * @param isMonitor
	 * @param isMd5Check
	 * @param isTransit
	 * @param sendToIp 默认sendToIp，后面发送文件时 若未设置，则用此值； 若 另外设置了sendToIp，则不会用此值
	 * @param sendToPort 默认sendToPort，后面发送文件时 若未设置，则用此值；  若另外设置了sendToIp，则不会用此值
	 * @param targetIp 目标主机的ip，默认targetIp
	 * @param targetPort 目标主机的port，默认 targetPort
	 * @param receiveMsgPort
	 */
	public FileClientConfig(String monitorPath, boolean isMonitor,
			boolean isMd5Check, boolean isTransit, String sendToIp,
			Integer sendToPort, String targetIp, Integer targetPort,
			Integer receiveMsgPort) {
		super();
		MonitorPath = monitorPath;
		this.isMonitor = isMonitor;
		
		if(isMonitor)
		{
			cacheName="cache"+MonitorPath.hashCode();
		}
		
		IsMd5Check = isMd5Check;
		this.isTransit = isTransit;
		this.sendToIp = sendToIp;
		this.sendToPort = sendToPort;
		this.targetIp = targetIp;
		this.targetPort = targetPort;
		this.receiveMsgPort = receiveMsgPort;
	}


	public String getMonitorPath() {
		return MonitorPath;
	}
	public void setMonitorPath(String monitorPath) {
		MonitorPath = monitorPath;
	}
	public int getSendTimeout() {
		return sendTimeout;
	}
	public void setSendTimeout(int soTimeout) {
		this.sendTimeout = soTimeout;
	}
	public boolean isIsMd5Check() {
		return IsMd5Check;
	}
	public void setIsMd5Check(boolean isMd5Check) {
		IsMd5Check = isMd5Check;
	}
	public boolean isTransit() {
		return isTransit;
	}
	public void setTransit(boolean isTransit) {
		this.isTransit = isTransit;
	}
	public String getSendToIp() {
		return sendToIp;
	}
	public void setSendToIp(String sendToIp) {
		this.sendToIp = sendToIp;
	}
	public Integer getSendToPort() {
		return sendToPort;
	}
	public void setSendToPort(Integer sendToPort) {
		this.sendToPort = sendToPort;
	}
	public String getTargetIp() {
		return targetIp;
	}
	public void setTargetIp(String targetIp) {
		this.targetIp = targetIp;
	}
	public Integer getTargetPort() {
		return targetPort;
	}
	public void setTargetPort(Integer targetPort) {
		this.targetPort = targetPort;
	}
	public Integer getReceiveMsgPort() {
		return receiveMsgPort;
	}
	public void setReceiveMsgPort(Integer receiveMsgPort) {
		this.receiveMsgPort = receiveMsgPort;
	}
	public String getTransferToHostParseInfo() {
		return transferToHostParseInfo;
	}
	public void setTransferToHostParseInfo(String transferToHostParseInfo) {
		this.transferToHostParseInfo = transferToHostParseInfo;
	}
	public boolean isMonitor() {
		return isMonitor;
	}
	public void setMonitor(boolean isMonitor) {
		this.isMonitor = isMonitor;
	}


	public int getFailResendCount() {
		return failResendCount;
	}


	public void setFailResendCount(int failResendCount) {
		this.failResendCount = failResendCount;
	}


	public String getCacheName() {
		return cacheName;
	}


	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}


	public boolean isThreadPoolSend() {
		return isThreadPoolSend;
	}


	public void setThreadPoolSend(boolean isThreadPoolSend) {
		this.isThreadPoolSend = isThreadPoolSend;
	}


	public int getBlockingQueueSize() {
		return blockingQueueSize;
	}


	public void setBlockingQueueSize(int blockingQueueSize) {
		this.blockingQueueSize = blockingQueueSize;
	}


	public int getSendThreadNum() {
		return sendThreadNum;
	}


	public void setSendThreadNum(int sendThreadNum) {
		this.sendThreadNum = sendThreadNum;
	}


	public int getTimerTaskPeriod() {
		return timerTaskPeriod;
	}


	public void setTimerTaskPeriod(int timerTaskPeriod) {
		this.timerTaskPeriod = timerTaskPeriod;
	}


	public boolean isAuthorityCheck() {
		return isAuthorityCheck;
	}


	public void setAuthorityCheck(boolean isAuthorityCheck) {
		this.isAuthorityCheck = isAuthorityCheck;
	}


	public String getAuthorityKey() {
		return authorityKey;
	}


	public void setAuthorityKey(String authorityKey) {
		this.authorityKey = authorityKey;
	}
	
	
	
	
	
}
