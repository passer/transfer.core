
package com.filetransfer.command;

import com.filetransfer.client.state.ServerReceiveControl;
import com.filetransfer.config.FileClientConfig;
import com.filetransfer.config.FileServerConfig;
import com.filetransfer.plugin.DemoServerFilePlugin;
import com.filetransfer.plugin.ServerFilePlugin;
import com.filetransfer.server.fileserver.FileReceiveServer;
import com.filetransfer.server.fileserver.callback.EdenFileReceiveCallBack;
import com.filetransfer.server.fileserver.callback.TransitFileReceiveCallBack;
import com.filetransfer.util.LogUtil;


/**
 * 功能说明: 文件接收启动类 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2014-7-25 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class ReceiveServerPulse extends Thread {

	private ServerReceiveControl serverReceiveControl;
	
	public ReceiveServerPulse(ServerReceiveControl serverReceiveControl)
	{
		if(serverReceiveControl==null)
		{
			throw new RuntimeException(ReceiveServerPulse.class.getName()+" serverReceiveControl不能为null ");
		}
		this.serverReceiveControl=serverReceiveControl;
		serverReceiveControl.init();
	}
	
	@Override
	public void run() {


		FileReceiveServer ser=null;
		try {
			/*if(GlobalProperties.IS_B_SERVER)
			{
				ser=new FileReceiveServer(GlobalProperties.B_RECEIVE_FILE_PORT,GlobalProperties.RECEIVE_FILE_SAVE_PATH);
				
			}else if(GlobalProperties.IS_C_SERVER)
			{
				ser=new FileReceiveServer(GlobalProperties.C_RECEIVE_FILE_PORT,GlobalProperties.C_RECEIVE_FILE_SAVE_PATH);
			}*/
			ser=new FileReceiveServer(serverReceiveControl);
			ser.service();
			
			
		} catch (Exception e) {
			LogUtil.err("初始化文件接收服务器失败!!!", e);
			e.printStackTrace();
		}
		
		
	}

	public static void main(String[] args) {
		
		//只作为接收主机	

		FileServerConfig fileServerConfig=new FileServerConfig("d:\\csavefile", 112, false);
		ServerReceiveControl serverReceiveControl=new ServerReceiveControl(fileServerConfig);
		serverReceiveControl.addServerFilePlugin(new ServerFilePlugin());
		serverReceiveControl.addServerFilePlugin(new DemoServerFilePlugin());
		ReceiveServerPulse ReceiveServerPulse=new ReceiveServerPulse( serverReceiveControl);
		ReceiveServerPulse.start();
		
/*		//作为中转主机
		FileServerConfig fileServerConfig=new FileServerConfig("d:\\csavefile", 111, true);
		ServerReceiveControl serverReceiveControl=new ServerReceiveControl(fileServerConfig);
		serverReceiveControl.addServerFilePlugin(new ServerFilePlugin());
		serverReceiveControl.addServerFilePlugin(new DemoServerFilePlugin());
		ReceiveServerPulse ReceiveServerPulse=new ReceiveServerPulse(serverReceiveControl);
		ReceiveServerPulse.start();*/
		
	}
	
}
