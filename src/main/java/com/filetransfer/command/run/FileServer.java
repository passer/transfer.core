package com.filetransfer.command.run;

import com.filetransfer.command.FileServerParser;
import com.filetransfer.command.ReceiveServerPulse;

public class FileServer {

	public void start() throws Exception
	{
		/*LogServerFilePlugin logServerFilePlugin=(LogServerFilePlugin)ApplicationContextProvider.getBean("LogServerFilePlugin");
		FileServerConfig fileServerConfig=new FileServerConfig("E:\\test\\file", 111, true);
		ServerReceiveControl serverReceiveControl=new ServerReceiveControl(fileServerConfig);
		//serverReceiveControl.addServerFilePlugin(new ServerFilePlugin());
		serverReceiveControl.addServerFilePlugin(logServerFilePlugin);*/
		FileServerParser fileServerParser=new FileServerParser();
		fileServerParser.parser();
		ReceiveServerPulse ReceiveServerPulse=new ReceiveServerPulse(fileServerParser.getServerReceiveControl());
		ReceiveServerPulse.start();
		
	}
	
	
	public static void main(String[] args) {
		

		
		FileServer fileServer=new FileServer();
		try {
			fileServer.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
