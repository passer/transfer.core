
package com.filetransfer.command.run;

import java.net.URL;
import java.util.Properties;

import com.filetransfer.command.FileServerParser;
import com.filetransfer.command.FileTransferClient;
import com.filetransfer.config.FileClientConfig;
import com.filetransfer.plugin.AuthorityClientFilePlugin;
import com.filetransfer.util.PropertiesUtil;


/**
 * 功能说明: 文件传输客户端启动例子 <br>
 * 开发人员: hanxuetong <br>
 * 开发时间: 2015-1-20 <br>
 * ======== ====== ============================================ <br>
 * 
 */
public class FileClientRunDemo {

	
	public static void main(String[] args) {
		URL url=FileClientRunDemo.class.getResource("/");
		String rootPath="";
		if(url!=null){
			rootPath=url.getPath();
		}
		
		Properties sysInfo=PropertiesUtil.readPropertiesFile(rootPath+"sysinfo.properties");
		
		String monitorPath=sysInfo.getProperty("monitorPath");
		
		boolean isMd5Check =PropertiesUtil.getPropertyBooleanValue(sysInfo.getProperty("isMd5Check"),true); 
		boolean isTransit=PropertiesUtil.getPropertyBooleanValue(sysInfo.getProperty("isTransit"),false); 
		String sendToIp=sysInfo.getProperty("sendToIp");
		Integer sendToPort=Integer.parseInt(sysInfo.getProperty("sendToPort"));
		
		FileClientConfig clientConfig=null;
		if(isTransit)  //中转
		{
			String targetIp=sysInfo.getProperty("targetIp");
			Integer targetPort=Integer.parseInt(sysInfo.getProperty("targetPort"));
			Integer receiveMsgPort=Integer.parseInt(sysInfo.getProperty("receiveMsgPort"));
			
			clientConfig=new FileClientConfig(monitorPath, true,isMd5Check,isTransit, sendToIp, sendToPort,targetIp,targetPort,receiveMsgPort);
				
		}else{ //不中转
			clientConfig=new FileClientConfig(monitorPath, true,isMd5Check,isTransit, sendToIp, sendToPort);
			
		}
		
		String timerTaskPeriod=sysInfo.getProperty("timerTaskPeriod");
		if(timerTaskPeriod!=null)
		{
			clientConfig.setTimerTaskPeriod(Integer.parseInt(timerTaskPeriod));
		}
		String sendTimeout=sysInfo.getProperty("sendTimeout");
		if(sendTimeout!=null)
		{
			clientConfig.setSendTimeout(Integer.parseInt(sendTimeout));
		}
		String failResendCount=sysInfo.getProperty("failResendCount");
		if(failResendCount!=null)
		{
			clientConfig.setFailResendCount(Integer.parseInt(failResendCount));
		}
		String cacheName=sysInfo.getProperty("cacheName");
		if(cacheName!=null)
		{
			clientConfig.setCacheName(cacheName);
		}
		
		
		
		FileTransferClient fileTransferClient=new FileTransferClient(clientConfig);
		
		
		boolean isAuthorityCheck =PropertiesUtil.getPropertyBooleanValue(sysInfo.getProperty("isAuthorityCheck"),true); 
		if(isAuthorityCheck)  //是否验证安全权限验证
		{
			String authorityKey=sysInfo.getProperty("authorityKey");
			clientConfig.setAuthorityCheck(isAuthorityCheck);
			clientConfig.setAuthorityKey(authorityKey);
			
			fileTransferClient.getTransferControl().addClientFilePlugin(new AuthorityClientFilePlugin(fileTransferClient.getTransferControl())); //添加验证拦截器
			
		}
		
		
		
		fileTransferClient.init();
		
		
		
		
	}
	
}
